/* collection_folder.vala
 *
 * Copyright 2020-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class CollectionFolder : Object, Collection {
    private Array<Song> songs;
    private Doctrina.BaseDeDatos db;
    private Doctrina.Infraestructura infra;
    private HashTable<string, bool> album_cover;
    public string music_directory { get; private set; }

    public CollectionFolder(string music_directory, Doctrina.BaseDeDatos db, Doctrina.Infraestructura infra) {
      this.songs = new Array<Song> ();
      this.album_cover = new HashTable<string, bool> (str_hash, str_equal);

      this.music_directory = music_directory;
      this.db = db;
      this.infra = infra;
    }

    public CollectionFolder.from_string(string data, Doctrina.BaseDeDatos db, Doctrina.Infraestructura infra) {
      var splited_data = data.split("|");
      var music_directory = "";

      if (splited_data.length > 1) {
        music_directory = splited_data[1];
      }

      this(music_directory, db, infra);
    }

    public async Array<Song> get_songs() {
      yield this.scan_directory_async(this.music_directory);

      this.loader_update("Searching for music files", "", (float) Jukebox.Pulse.STOP);

      return this.songs;
    }

    private async void scan_directory_async(string directory) {
      string name = "";
      string status = "Searching for music files";

      try {
        Dir dir = Dir.open(directory, 0);

        while ((name = dir.read_name()) != null) {
          string path = Path.build_filename(directory, name);
          this.loader_update(status, path.replace(this.music_directory, "..."), (float) Jukebox.Pulse.START);
          Idle.add(this.scan_directory_async.callback);
          yield;

          if (FileUtils.test(path, FileTest.IS_REGULAR)) {
            try {
              var discoverer = new Gst.PbUtils.Discoverer((Gst.ClockTime) 10 * Gst.SECOND);
              var uri = "file:%s".printf(Uri.escape_string(path.replace("\\", "/"), ":/"));
              var info = discoverer.discover_uri(uri);
              var song = this.get_song_from_discoverer(info, path);

              this.songs.append_val(song);
            } catch (Error err) {
              stderr.printf(err.message);
            }
          }

          if (FileUtils.test(path, FileTest.IS_DIR)) {
            yield this.scan_directory_async(path);
          }
        }
      } catch (FileError err) {
        stderr.printf(err.message);
      }
    }

    private Song get_song_from_discoverer(Gst.PbUtils.DiscovererInfo info, string path) {
      var streams = info ? .get_streams(typeof (Gst.PbUtils.DiscovererAudioInfo));
      var stream = streams ? .nth_data(0) as Gst.PbUtils.DiscovererStreamInfo;
      var tags = stream ? .get_tags();

      var album = "";
      var artist = "";
      var title = "";
      uint track = 0;
      var length = (int) (info.get_duration() / Gst.SECOND);
      Gdk.Pixbuf cover = null;
      tags?.foreach((list, tag) => {
        switch (tag) {
          case "title" :
            tags.get_string(tag, out title);
            title = title.replace("\"", "");
            break;
          case "album" :
            tags.get_string(tag, out album);
            album = album.replace("\"", "");
            break;
          case "artist" :
            tags.get_string(tag, out artist);
            artist = artist.replace("\"", "");
            break;
          case "track-number" :
            tags.get_uint(tag, out track);
            break;
          case "image":
            if (album != "" && this.album_cover.contains(album)) {
              break;
            }

            Gst.Sample sample;
            tags.get_sample(tag, out sample);
            var caps = sample.get_caps();

            for (int i = 0; i < caps.get_size(); i++) {
              unowned Gst.Structure structure = caps.get_structure(i);
              if (structure.get_name().contains("image/")) {
                var mime = structure.get_name();
                try {
                  var buffer = sample.get_buffer();
                  uint8[] data = null;
                  buffer.extract_dup(0, buffer ? .get_size(), out data);
                  if (data != null) {
                    var loader = new Gdk.PixbufLoader.with_mime_type(mime);
                    loader.write(data);
                    loader.close();
                    cover = loader.get_pixbuf();
                    if (album != "") {
                      this.album_cover.insert(album, true);
                    }
                  }
                } catch (Error e) {
                  debug("error creating pixbuf: %s", e.message);
                }
              }
            }
            break;
        }
      });

      var uri = "file:%s".printf(Uri.escape_string(path.replace("\\", "/"), ":/"));
      return (new SongBuilder())
              .with_title(title)
              .with_length(length)
              .with_album(album)
              .with_artist(artist)
              .with_track((int) track)
              .with_uri(uri)
              .with_cover(cover)
              .build();
    }

    public async bool is_song_in_collection_async(string uri) {
      var in_collection = false;
      if (uri.contains("file:/")) {
        var file = File.new_for_uri(uri);
        var path = file ? .get_path();
        in_collection = (path.length > this.music_directory.length &&
                         path.substring(0, this.music_directory.length) == this.music_directory);
      }
      return in_collection;
    }

    public string to_string() {
      return "%s|%s".printf(CollectionType.FOLDER.to_string(), this.music_directory);
    }
  }
}
