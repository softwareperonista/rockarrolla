/* application.vala
 *
 * Copyright 2022 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Application : Adw.Application {
    public Application() {
      Object(application_id: "ar.com.softwareperonista.Rockarrolla", flags : ApplicationFlags.FLAGS_NONE);

      var style_manager = this.get_style_manager();
      style_manager.set_color_scheme(Adw.ColorScheme.PREFER_DARK);
    }

    public override void activate() {
      base.activate();
      var win = this.active_window;
      if (win == null) {
        win = new Rockarrolla.Window(this);
      }
      win.present();
    }
  }
}
