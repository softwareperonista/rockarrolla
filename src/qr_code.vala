/* qr_code.vala
 *
 * Copyright 2022 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class QRCode : Object {
    private Gdk.Pixbuf pixbuf;

    public QRCode(string text) {
      var qrcode = new Qrencode.QRcode.encodeString(text, 0, Qrencode.EcLevel.L, Qrencode.Mode.B8, 1);

      var pixbuf_data = new uint8[(qrcode.width + 2) * (qrcode.width + 2) * 3];
      var counter = 0;
      for (int i = 0; i < (qrcode.width + 2); i++) {
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
      }
      for (int i = 0; i < qrcode.width; i++) {
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
        for (int j = 0; j < qrcode.width; j++) {
          if (qrcode.data[i * qrcode.width + j] % 2 == 1) {
            pixbuf_data[counter++] = 0;
            pixbuf_data[counter++] = 0;
            pixbuf_data[counter++] = 0;
          } else {
            pixbuf_data[counter++] = 255;
            pixbuf_data[counter++] = 255;
            pixbuf_data[counter++] = 255;
          }
        }
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
      }
      for (int i = 0; i < (qrcode.width + 2); i++) {
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
        pixbuf_data[counter++] = 255;
      }

      var pixbuf = new Gdk.Pixbuf.from_data(pixbuf_data, Gdk.Colorspace.RGB, false, 8, qrcode.width + 2, qrcode.width + 2, (qrcode.width + 2) * 3);
      this.pixbuf = pixbuf;
    }

    public Gdk.Pixbuf get_pixbuf(int size) {
      Gdk.Pixbuf pixbuf = this.pixbuf;
      if (size > 0) {
        var scaled = this.pixbuf.scale_simple(512, 512, Gdk.InterpType.NEAREST);
        pixbuf = scaled;
      }

      return pixbuf;
    }
  }
}
