/* Jukebox.vala
 *
 * Copyright 2020-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Jukebox : Object {
    public Doctrina.BaseDeDatos db { get; private set; }
    public Doctrina.Infraestructura infra { get; private set; }
    public Array<PlaylistSong> playlist { get; private set; }
    public string covers_dir { get; private set; }
    private Player player;
    private int time_left;
    private int time_elapsed;
    private Array<string> songs_path;
    private Array<string> initials;
    private Settings settings;
    private CollectionLoader collection_loader;
    private string add_playlist_song_id;
    private Array<Album> albums;
    private Server server;
    private bool restoring_playlist;
    private int playlist_last_update;
    private HashTable<string, Array<string>> device_waitinglist;

    public signal void albums_load_start();
    public signal void album_loaded(Album album);
    public signal void albums_load_end();
    public signal void playlist_empty();
    public signal void playlist_update_songs(ListStore songs);
    public signal void playlist_update_time(int time_left, int time_elapsed);
    public signal void idle_cover_update(string title, string artist, string path, double length);
    public signal void loader_update(string status, string action, float progress);
    public signal void collection_reloaded();
    public signal void update_songs_per_coin(string songs_per_coin, string coin_cost);
    public signal void updated_songs_left(string songs_left);
    public signal void message(string text, Jukebox.MessageType type = Jukebox.MessageType.INFO);
    public signal void party_mode(bool enabled, Gdk.Pixbuf? qr_code);

    public Jukebox() {
      this.playlist = new Array<PlaylistSong> ();
      this.player = new Player();
      this.songs_path = new Array<string> ();
      this.initials = new Array<string> ();
      this.albums = new Array<Album> ();
      this.add_playlist_song_id = "";
      this.restoring_playlist = false;
      this.playlist_last_update = 0;
      this.device_waitinglist = new HashTable<string, Array<string>> (str_hash, str_equal);

      this.infra = new Doctrina.Infraestructura(
                                                "rockarrolla",
                                                "6",
                                                "resource:///ar/com/softwareperonista/rockarrolla/db/base_de_datos_definicion.sql");
      this.db = new Doctrina.BaseDeDatos("db.sqlite",
                                         this.infra);

      this.covers_dir = this.infra.directorio_de_configuracion() + "/albums_covers/";
      if (!Doctrina.Utiles.existe_path(this.infra.directorio_de_configuracion() + "/albums_covers")) {
        Doctrina.Utiles.crear_directorio(this.infra.directorio_de_configuracion() + "/albums_covers");
      }

      this.time_left = 0;
      this.time_elapsed = 0;

      this.settings = new Settings("ar.com.softwareperonista.Rockarrolla");
      this.settings.changed.connect(this.settings_changed);

      this.player.playing.connect(this.update_time);
      this.player.play_finished.connect(this.play_finished);
      this.player.error.connect((title) => {
        this.message("Unable to play %s".printf(title), Jukebox.MessageType.ERROR);
      });

      this.collection_loader = new CollectionLoader(this.db, this.infra);
      this.collection_loader.loader_update.connect((status, action, progress) => { this.loader_update(status, action, progress); });

      this.settings.changed.connect((key) => {
        if (key == "party-mode") {
          this.check_party_mode();
        }
      });
    }

    private void settings_changed(string key) {
      if (key == "songs-per-coin" || key == "coin-cost") {
        int songs = this.settings.get_int("songs-per-coin");
        string coin = this.settings.get_string("coin-cost");

        this.update_songs_per_coin(songs.to_string(), coin);
      }

      if (key == "songs-left") {
        int songs_left = this.settings.get_int("songs-left");

        this.updated_songs_left(songs_left.to_string());
      }
    }

    public void update_info_labels() {
      this.settings_changed("coin-cost");
      this.settings_changed("songs-left");
    }

    public void load_albums() {
      this.albums_load_start();

      this.load_albums_async.begin((obj, res) => {
        this.albums_load_end();
      });
    }

    private async void load_albums_async() {
      var albums_db = this.db.select("albums", "id,name,artist", "ORDER BY artist ASC, name ASC");
      this.initials = new Array<string> ();

      for ( int i = 0; i < albums_db.length; i++ ) {
        var album_db = albums_db.index(i).split("|");

        var id = album_db[0];
        var name = album_db[1];
        var artist = album_db[2];

        var cover = generate_cover_path(artist, name);

        if (!Doctrina.Utiles.existe_path(cover)) {
          cover = "";
        }

        var album = new Album(id, name, artist, cover);
        this.get_album_songs(album);

        var initial = album.get_initial();
        if (this.initials.length == 0) {
          this.initials.append_val(initial);
        } else {
          if (this.initials.index(this.initials.length - 1) != initial) {
            this.initials.append_val(initial);
          }
        }

        this.albums.append_val(album);
        this.album_loaded(album);
        Idle.add(this.load_albums_async.callback);
        yield;
      }
    }

    public Array<Album> get_albums() {
      return this.albums;
    }

    private void get_album_songs(Album album) {
      var songs_db = this.db.select("songs", "id,title,length,path", "WHERE album = \"%s\" ORDER BY track ASC".printf(album.id));

      for ( int i = 0; i < songs_db.length; i++ ) {
        var song_db = songs_db.index(i).split("|");

        var id = song_db[0];
        var title = song_db[1];
        var length = int.parse(song_db[2]);
        var uri = song_db[3];

        var song = (new SongBuilder())
           .with_id(id)
           .with_title(title)
           .with_length(length)
           .with_uri(uri)
           .build();

        album.add_song(song);
      }

      album.set_total_songs(songs_db.length);
    }

    public void play() {
      if (this.playlist.length > 0) {
        if (this.player.state == Player.State.STOPPED) {
          this.player.play_song(this.playlist.index(0).song);
          this.update_cover();
        }
      } else {
        this.playlist_last_update = this.get_timestamp();
        this.playlist_empty();
      }
    }

    public void add_songs() {
      int songs_per_coin = this.settings.get_int("songs-per-coin");
      int songs_left = this.settings.get_int("songs-left");

      this.settings.set_int("songs-left", songs_left + songs_per_coin);
      this.message(_("Coin added"), Jukebox.MessageType.INFO);
    }

    public bool add_song(string id, string device_id = "") {
      bool can_add = true;
      if (this.settings.get_int("songs-left") > 0 || this.settings.get_boolean("party-mode")) {
        if (!this.playlist_add_song(id, device_id)) {
          can_add = false;
        }
      } else {
        this.message(_("%s songs left to choose").printf("0"), Jukebox.MessageType.ERROR);
        can_add = false;
      }

      return can_add;
    }

    private async void insert_song_in_playlist_db_async(string id) {
      this.db.insert("playlist", "song", "'%s'".printf(id));
    }

    private bool playlist_add_song(string id, string device_id = "") {
      debug("id: %s", id);
      debug("device_id: %s", device_id);
      this.add_playlist_song_id = id;
      var ret = true;

      if (this.song_in_playlist(id)) {
        ret = false;
        if (!this.settings.get_boolean("party-mode")) {
          this.message(_("This song is already in the playlist"), Jukebox.MessageType.ERROR);
        }
      }

      return ret;
    }

    public void confirm_add_song_playlist(string device_id = "") {
      if (device_id != "" && this.device_has_song_in_playlist(device_id)) {
        debug("add song to waiting list");
        this.add_song_to_waitinglist(device_id, this.add_playlist_song_id);
        this.playlist_last_update = this.get_timestamp();
        this.update_playlist();
        return;
      }

      if (this.add_playlist_song_id != "") {
        this.insert_song_in_playlist_db_async.begin(this.add_playlist_song_id, () => {
          if (!this.settings.get_boolean("party-mode")) {
            this.message(_("Song added to playlist"), Jukebox.MessageType.INFO);
          }
        });

        this.add_song_to_playlist(device_id);

        if (!this.settings.get_boolean("party-mode")) {
          int songs_left = this.settings.get_int("songs-left");
          this.settings.set_int("songs-left", songs_left - 1);
        } else {
          if (device_id != "") {
            var song_playlist_id = this.db.select("playlist", "id", "WHERE song = \"%s\"".printf(this.add_playlist_song_id)).index(0);
            var client_id = this.db.select("clients", "id", "WHERE device = \"%s\"".printf(device_id)).index(0);
            this.db.insert("client_playlist_song", "client,playlist_song", "\"%s\",\"%s\"".printf(client_id, song_playlist_id));
          }
        }
      }
    }

    public void unset_song_to_add_id() {
      this.add_playlist_song_id = "";
    }

    public void add_song_to_playlist(string device_id) {
      var playlist_song = new PlaylistSong(this.create_song(this.add_playlist_song_id), device_id);

      this.playlist.append_val(playlist_song);
      this.time_left += playlist_song.song.length;
      this.play();
      this.update_playlist();
      this.playlist_last_update = this.get_timestamp();
    }

    private bool device_has_song_in_playlist(string device_id) {
      var found = false;
      for ( uint i = 0; i < this.playlist.length; i++ ) {
        var d = this.playlist.index(i).device_id;
        if (d == device_id) {
          found = true;
          break;
        }
      }

      return found;
    }

    private bool song_in_playlist(string id) {
      var found = false;
      for ( uint i = 0; i < this.playlist.length; i++ ) {
        var song = this.playlist.index(i).song;
        if (song != null && song.id == id) {
          found = true;
          break;
        }
      }

      if (!found) {
        var devices = this.device_waitinglist.get_keys();

        for ( uint i = 0; i < devices.length(); i++ ) {
          var device = devices.nth_data(i);
          var waiting_list = this.device_waitinglist.get(device);

          for (uint j = 0; j < waiting_list.length; j++) {
            if (waiting_list.index(j) == id) {
              found = true;
              break;
            }
          }

          if (found) {
            break;
          }
        }
      }

      return found;
    }

    public Song create_song(string id) {
      var song_db = this.db.select("songs,albums", "title,length,path,artist,name", "WHERE songs.id = '%s' AND songs.album = albums.id".printf(id)).index(0).split("|");

      var title = song_db[0];
      var length = int.parse(song_db[1]);
      var uri = song_db[2];
      var artist = song_db[3];
      var album_name = song_db[4];
      var cover = generate_cover_path(artist, album_name);

      var song = (new SongBuilder())
         .with_id(id)
         .with_title(title)
         .with_length(length)
         .with_uri(uri)
         .with_artist(artist)
         .with_cover_filename(cover)
         .build();

      return song;
    }

    private void play_finished() {
      var device_id = this.playlist.index(0).device_id;
      var id = this.db.select("playlist", "id", "WHERE song = '%s'".printf(this.playlist.index(0).song.id)).index(0);
      this.db.del("playlist", "WHERE id = '%s'".printf(id));
      this.db.del("client_playlist_song", "WHERE playlist_song = '%s'".printf(id));
      this.time_left -= this.playlist.index(0).song.length;
      this.playlist.remove_index(0);
      this.playlist_last_update = this.get_timestamp();
      this.time_elapsed = 0;
      if (time_left != 0) {
        this.update_time(0);
      }

      if (this.settings.get_boolean("party-mode")) {
        debug("device_id: %s", device_id);
        debug("this.device_waitinglist.contains(device_id): %s", this.device_waitinglist.contains(device_id).to_string());
        debug("this.device_waitinglist.get(device_id).length: %s", this.device_waitinglist.get(device_id) ? .length ? .to_string());
        if (device_id != "" &&
            this.device_waitinglist.contains(device_id) &&
            this.device_waitinglist.get(device_id) ? .length > 0) {
          var song_id = this.device_waitinglist.get(device_id).index(0);
          debug("song_id: %s", song_id);
          this.add_playlist_song_id = song_id;
          this.confirm_add_song_playlist(device_id);
          this.db.del("waitinglists", "WHERE song = '%s'".printf(song_id));
          this.time_left -= this.create_song(song_id).length;
          this.device_waitinglist.get(device_id).remove_index(0);
        }
      }

      this.update_playlist();
      this.play();
    }

    public void restore_playlist() {
      this.restoring_playlist = true;
      var songs_id = this.db.select("playlist", "song", "");


      for ( int i = 0; i < songs_id.length; i++ ) {
        var device_id = "";
        if (this.settings.get_boolean("party-mode")) {
          var song_playlist_id = this.db.select("playlist", "id", "WHERE song = \"%s\"".printf(songs_id.index(i))).index(0);
          var client = this.db.select("client_playlist_song", "client", "WHERE playlist_song = '%s'".printf(song_playlist_id));
          var client_id = client.length > 0 ? client.index(0) : "";
          if (client_id != "") {
            var device = this.db.select("clients", "device", "WHERE id = '%s'".printf(client_id));
            device_id = device.length > 0 ? device.index(0) : "";
          }
        }
        if (this.playlist_add_song(songs_id.index(i), device_id)) {
          this.add_song_to_playlist(device_id);
        }
      }
      this.restoring_playlist = false;
    }

    private void update_playlist() {
      var liststore = new ListStore(typeof (Song));

      for ( int i = 0; i < this.playlist.length; i++ ) {
        liststore.append(this.playlist.index(i).song);
      }

      if (this.settings.get_boolean("party-mode")) {
        var global_waitinglist = this.get_global_waitinglist();
        for ( int i = 0; i < global_waitinglist.length; i++ ) {
          liststore.append(global_waitinglist.index(i));
        }
      }

      this.playlist_update_songs(liststore);
    }

    private void update_time(int time_elapsed) {
      this.time_elapsed = time_elapsed;
      var real_time_left = this.time_left - this.time_elapsed;
      this.playlist_update_time(real_time_left, time_elapsed);
    }

    private void update_cover() {
      if (this.playlist.length > 0) {
        var song = this.playlist.index(0).song;
        var title = song.title;
        var length = song.length;
        var album = this.db.select("songs, albums",
                                   "artist,name",
                                   "WHERE songs.id = '%s' AND album = albums.id".printf(song.id)).index(0).split("|");
        var artist = album[0];
        var name = album[1];

        var cover = generate_cover_path(artist, name);

        if (!Doctrina.Utiles.existe_path(cover)) {
          cover = "";
        }

        this.idle_cover_update(title, artist, cover, length);
      } else {
        this.idle_cover_update("", "", "", 0);
      }
    }

    public void toggle_pause() {
      this.player.toggle_pause();
    }

    public void skip_song() {
      if (this.player.state != Player.State.STOPPED) {
        this.player.stop();
      }
    }

    private string generate_cover_path(string artist, string name) {
      return this.covers_dir + CoverFetcher.sha1(artist + "_" + name) + ".png";
    }

    public bool is_playing() {
      return this.player.state == Player.State.PLAYING;
    }

    public void reload_collection() {
      this.collection_loader.reload_collection_async.begin(() => {
        this.collection_reloaded();
        this.load_albums();
      });
    }

    public Array<string> get_initials() {
      return this.initials;
    }

    public void check_party_mode() {
      var enabled = this.settings.get_boolean("party-mode");
      if (enabled) {
        this.start_server();
        this.initialize_waitinglists();
        this.party_mode(true, new QRCode(this.server.get_connect_json()).get_pixbuf(256));
      } else {
        this.party_mode(false, null);
        this.stop_server();
      }
    }

    private void start_server() {
      var port = this.settings.get_int("party-mode-server-port");
      var jukebox = this;
      this.server = new Server(port, ref this.settings, ref jukebox);

      this.server.add_handlers();

      if (!this.server.start()) {
        this.settings.set_boolean("party-mode", false);
      }
    }

    private void stop_server() {
      this.server ? .stop();
    }

    private void initialize_waitinglists() {
      var devices = this.db.select("clients", "device", "");
      GLib.message("creating waitinglist for %u devices", devices.length);

      for (uint i = 0; i < devices.length; i++) {
        var device_id = devices.index(i);
        this.device_waitinglist.insert(device_id, new GLib.Array<string> ());

        var device_songs = this.db.select("waitinglists", "song", "WHERE device='%s'".printf(device_id));
        for ( uint j = 0; j < device_songs.length; j++ ) {
          this.add_song_to_waitinglist(device_id, device_songs.index(j), false);
        }
      }
    }

    private void add_song_to_waitinglist(string device_id, string song_id, bool insert_in_db = true) {
      this.device_waitinglist.get(device_id) ? .append_val(song_id);
      this.time_left += this.create_song(song_id).length;
      if (insert_in_db) {
        this.db.insert("waitinglists", "device,song", "'%s', '%s'".printf(device_id, song_id));
      }
    }

    public void add_client(string device_id) {
      var db_client_id = this.db.select("clients", "id", "WHERE device = \"%s\"".printf(device_id));
      if (db_client_id.length == 0) {
        this.db.insert("clients", "device", "\"%s\"".printf(device_id));
      } else {
        debug("device '%s' already in db".printf(device_id));
      }

      if (!this.device_waitinglist.contains(device_id)) {
        this.device_waitinglist.insert(device_id, new GLib.Array<string> ());
      }
    }

    public int get_time_left() {
      return this.time_left - this.time_elapsed;
    }

    public int get_time_elapsed() {
      return this.time_elapsed;
    }

    public int get_playlist_last_update() {
      return this.playlist_last_update;
    }

    public int get_timestamp() {
      return (int) (GLib.get_real_time() / 1000 / 1000);
    }

    public Array<Song> get_device_waitinglist(string device_id) {
      var waitinglist = new Array<Song> ();

      if (this.device_waitinglist.contains(device_id)) {
        var device_wl = this.device_waitinglist.get(device_id);

        for (uint i = 0; i < device_wl.length; i++) {
          var song = this.create_song(device_wl.index(i));
          waitinglist.append_val(song);
        }
      }

      return waitinglist;
    }

    public Array<Song> get_global_waitinglist() {
      var global_playlist = new Array<Song> ();

      var devices = this.device_waitinglist.get_keys();

      for ( uint i = 0; i < devices.length(); i++ ) {
        var device = devices.nth_data(i);
        debug("adding songs from device, %s", device);
        var waiting_list = device_waitinglist.get(device);

        for (uint j = 0; j < waiting_list.length; j++) {
          var song = this.create_song(waiting_list.index(j));
          global_playlist.append_val(song);
        }
      }


      for (uint i = 0; i < global_playlist.length; i++) {
        debug("song id: %s", global_playlist.index(i).id);
      }
      return global_playlist;
    }

    public bool remove_song_waitinglist(string song_id, string device_id) {
      bool removed = false;

      var waitinglist = this.device_waitinglist.get(device_id);

      for (uint i = 0; i < waitinglist.length; i++) {
        if (waitinglist.index(i) == song_id) {
          waitinglist.remove_index(i);
          this.time_left -= this.create_song(song_id).length;
          removed = this.db.del("waitinglists", "WHERE device='%s' AND song='%s'".printf(device_id, song_id));
          debug("Song ID: '%s' from device ID: '%S' removed from waitinglist".printf(song_id, device_id));
          this.playlist_last_update = this.get_timestamp();
          this.update_playlist();
          break;
        }
      }

      return removed;
    }

    public enum Pulse {
      START = -1,
      STOP = -2
    }

    public enum MessageType {
      INFO,
      ERROR
    }
  }
}
