/* page_configuration.vala
 *
 * Copyright 2019-2021 Andres Fernández <andres@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/page_configuration.ui")]
  public class PageConfiguration : Adw.Bin {
    [GtkChild]
    private unowned Gtk.ListBox listbox_collections;
    [GtkChild]
    private unowned Gtk.Entry entry_coin_cost;
    [GtkChild]
    private unowned Gtk.SpinButton spin_button_songs_per_coin;
    [GtkChild]
    private unowned Gtk.Switch switch_fullscreen_mode_default;
    [GtkChild]
    private unowned Gtk.Switch switch_party_mode;
    [GtkChild]
    private unowned Gtk.Entry entry_server_name;
    [GtkChild]
    private unowned Gtk.Entry entry_lan_ip;
    [GtkChild]
    private unowned Gtk.Entry entry_server_port;
    [GtkChild]
    private unowned Gtk.Entry entry_auth_key;
    [GtkChild]
    private unowned KeyBinding key_binding_up;
    [GtkChild]
    private unowned KeyBinding key_binding_down;
    [GtkChild]
    private unowned KeyBinding key_binding_left;
    [GtkChild]
    private unowned KeyBinding key_binding_right;
    [GtkChild]
    private unowned KeyBinding key_binding_play;
    [GtkChild]
    private unowned KeyBinding key_binding_coin;
    [GtkChild]
    private unowned KeyBinding key_binding_skip;
    [GtkChild]
    private unowned KeyBinding key_binding_pause;
    [GtkChild]
    private unowned KeyBinding key_binding_configuration;
    [GtkChild]
    private unowned KeyBinding key_binding_fullscreen_mode;
    [GtkChild]
    private unowned Gtk.MenuButton menu_button_collection_add;
    private GLib.SimpleActionGroup collection_action_gruop;
    private Settings settings;

    construct {
      this.settings = new Settings("ar.com.softwareperonista.Rockarrolla");

      this.collection_action_gruop = new GLib.SimpleActionGroup();
      this.collection_new_menu_create();
      this.menu_button_collection_add.insert_action_group("collection", this.collection_action_gruop);

      this.load_settings();
    }

    private void collection_new_menu_create() {
      var action = new SimpleAction("add_folder", null);
      action.set_enabled(true);
      action.activate.connect(() => { this.collection_folder_add_row(null); });
      this.collection_action_gruop.add_action(action);

      action = new SimpleAction("add_subsonic", null);
      action.set_enabled(true);
      action.activate.connect(() => { this.collection_subsonic_add_row(null); });
      this.collection_action_gruop.add_action(action);
    }

    public void load_settings() {
      var collections = this.settings.get_strv("collections");
      this.set_collections(collections);

      var coin_cost = this.settings.get_string("coin-cost");

      this.entry_coin_cost.set_text(coin_cost);

      var songs_per_coin = this.settings.get_int("songs-per-coin");

      this.spin_button_songs_per_coin.set_value((double) songs_per_coin);

      var fullscreen_mode_default = this.settings.get_boolean("fullscreen-mode-default");

      this.switch_fullscreen_mode_default.set_active(fullscreen_mode_default);

      this.key_binding_up.set_keyval(this.settings.get_uint("key-up"));
      this.key_binding_down.set_keyval(this.settings.get_uint("key-down"));
      this.key_binding_left.set_keyval(this.settings.get_uint("key-left"));
      this.key_binding_right.set_keyval(this.settings.get_uint("key-right"));
      this.key_binding_play.set_keyval(this.settings.get_uint("key-play"));
      this.key_binding_coin.set_keyval(this.settings.get_uint("key-coin"));
      this.key_binding_skip.set_keyval(this.settings.get_uint("key-skip"));
      this.key_binding_pause.set_keyval(this.settings.get_uint("key-pause"));
      this.key_binding_configuration.set_keyval(this.settings.get_uint("key-configuration"));
      this.key_binding_fullscreen_mode.set_keyval(this.settings.get_uint("key-fullscreen-mode"));

      this.settings.bind("party-mode", this.switch_party_mode, "active", SettingsBindFlags.DEFAULT);

      var server_name = this.settings.get_string("party-mode-server-name");
      this.entry_server_name.set_text(server_name);

      var lan_ip = this.settings.get_string("party-mode-lan-ip");
      this.entry_lan_ip.set_text(lan_ip);

      var port = this.settings.get_int("party-mode-server-port");
      this.entry_server_port.set_text("%u".printf(port));

      var auth_key = this.settings.get_string("party-mode-auth-key");
      this.entry_auth_key.set_text(auth_key);
    }

    private void set_collections(owned string[] collections) {
      if (collections.length == 0) {
        collections = { "" };
      }

      for ( int i = 0; i < collections.length; i++ ) {
        this.new_collection_row(collections[i]);
      }
    }

    private void new_collection_row(string data) {
      var type = CollectionType.parse(data);

      switch (type) {
      case CollectionType.SUBSONIC:
        this.collection_subsonic_add_row(data);
        break;
      default:
        this.collection_folder_add_row(data);
        break;
      }
    }

    private void save_collections() {
      string[] collections = {};
      var child = this.listbox_collections.get_first_child() as Gtk.ListBoxRow;
      while (child != null) {
        var collection = child.get_child() as CollectionRow;

        if (collection != null) {
          collections += collection.to_string();
        }

        child = child.get_next_sibling() as Gtk.ListBoxRow;
      }

      this.settings.set_strv("collections", collections);
    }

    private void on_row_deleted() {
      this.save_collections();
      this.set_collection_rows_delete_button();
    }

    private void set_collection_rows_delete_button() {
      CollectionRow collection_row;

      if (this.listbox_collections.get_first_child().get_next_sibling() == null) {
        collection_row = this.listbox_collections.get_first_child().get_first_child() as CollectionRow;
        collection_row.set_visible_delete_button(false);
      } else {
        int i = 0;

        while (this.listbox_collections.get_row_at_index(i) != null) {
          collection_row = this.listbox_collections.get_row_at_index(i).get_first_child() as CollectionRow;
          collection_row?.set_visible_delete_button(true);

          i++;
        }
      }
    }

    private void collection_folder_add_row(string? data) {
      this.collection_add_row(new CollectionRowFolder(), data);
    }

    private void collection_subsonic_add_row(string? data) {
      this.collection_add_row(new CollectionRowSubsonic(), data);
    }

    private void collection_add_row(CollectionRow new_collection, string? data) {
      if (data != null) {
        new_collection.from_string(data);
      }
      new_collection.collection_changed.connect(this.save_collections);

      this.listbox_collections.append(new_collection);
      var last_row = this.listbox_collections.get_last_child() as Gtk.ListBoxRow;
      last_row.destroy.connect(this.on_row_deleted);
      this.set_collection_rows_delete_button();
    }

    [GtkCallback]
    private void save_coin_cost() {
      var coin_cost = this.entry_coin_cost.get_text();

      settings.set_string("coin-cost", coin_cost);
    }

    [GtkCallback]
    private void save_songs_per_coin() {
      var songs_per_coin = this.spin_button_songs_per_coin.get_value_as_int();

      settings.set_int("songs-per-coin", songs_per_coin);
    }

    [GtkCallback]
    private void save_fullscreen_mode_default() {
      settings.set_boolean("fullscreen-mode-default", this.switch_fullscreen_mode_default.get_active());
    }

    [GtkCallback]
    private void save_server_name() {
      var server_name = this.entry_server_name.get_text();

      settings.set_string("party-mode-server-name", server_name);
    }

    [GtkCallback]
    private void save_lan_ip() {
      var lan_ip = this.entry_lan_ip.get_text();

      settings.set_string("party-mode-lan-ip", lan_ip);
    }

    [GtkCallback]
    private void save_server_port() {
      var server_port = this.entry_server_port.get_text();

      settings.set_int("party-mode-server-port", int.parse(server_port));
    }

    [GtkCallback]
    private void save_auth_key() {
      var auth_key = this.entry_auth_key.get_text();

      settings.set_string("party-mode-auth-key", auth_key);
    }

    [GtkCallback]
    private void save_key_up(uint keyval) {
      this.settings.set_uint("key-up", keyval);
    }

    [GtkCallback]
    private void save_key_down(uint keyval) {
      this.settings.set_uint("key-down", keyval);
    }

    [GtkCallback]
    private void save_key_left(uint keyval) {
      this.settings.set_uint("key-left", keyval);
    }

    [GtkCallback]
    private void save_key_right(uint keyval) {
      this.settings.set_uint("key-right", keyval);
    }

    [GtkCallback]
    private void save_key_play(uint keyval) {
      this.settings.set_uint("key-play", keyval);
    }

    [GtkCallback]
    private void save_key_coin(uint keyval) {
      this.settings.set_uint("key-coin", keyval);
    }

    [GtkCallback]
    private void save_key_skip(uint keyval) {
      this.settings.set_uint("key-skip", keyval);
    }

    [GtkCallback]
    private void save_key_pause(uint keyval) {
      this.settings.set_uint("key-pause", keyval);
    }

    [GtkCallback]
    private void save_key_configuration(uint keyval) {
      this.settings.set_uint("key-configuration", keyval);
    }

    [GtkCallback]
    private void save_key_fullscreen_mode(uint keyval) {
      this.settings.set_uint("key-fullscreen-mode", keyval);
    }
  }
}
