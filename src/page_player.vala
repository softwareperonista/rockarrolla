/* page_configuration.vala
 *
 * Copyright 2019-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/page_player.ui")]
  public class PagePlayer : Gtk.Widget {
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned AlbumsViewer albums_viewer;
    [GtkChild]
    private unowned SongsViewer songs_viewer;
    [GtkChild]
    private unowned Playlist playlist;
    [GtkChild]
    private unowned Gtk.Label label_songs_per_coin;
    [GtkChild]
    private unowned Gtk.Label label_songs_left;
    [GtkChild]
    private unowned PageIdleCover page_idle_cover;
    [GtkChild]
    private unowned Gtk.Overlay overlay;
    private Gtk.SizeGroup size_group_songs_viewer_playlist;
    private DialogInitials initials;
    public DialogSongSelected dialog_song_selected { get; private set; }
    public state page_state { private set; get; }
    public signal void add_song_response();

    construct {
      this.set_name("pageplayer");
      this.playlist_update_time(0, 0);
      this.page_state = state.PLAYER;

      this.size_group_songs_viewer_playlist = new Gtk.SizeGroup(Gtk.SizeGroupMode.VERTICAL);
      this.songs_viewer.set_label_artist_album_size_group(this.size_group_songs_viewer_playlist);
      this.playlist.set_label_time_left_size_group(this.size_group_songs_viewer_playlist);
    }

    public void albums_load_start() {
      this.albums_viewer.albums_load_start();
    }

    public void load_album(Album album) {
      this.albums_viewer.load_album(album);
    }

    public void albums_load_end() {
      this.albums_viewer.albums_load_end();
    }

    private void move_album_selection(PagePlayer.move move) {
      this.albums_viewer.move_album_selection(move);
    }

    private void move_song_selection(PagePlayer.move move) {
      this.songs_viewer.move_song_selection(move);
    }

    public string get_selected_song_id() {
      return this.songs_viewer.get_selected_song_id();
    }

    [GtkCallback]
    private void load_album_songs() {
      this.songs_viewer.load_album_songs(this.albums_viewer.get_album_selected());
    }

    public void set_songs_per_coin(string songs_per_coin, string coin_cost) {
      string template = _("%s songs for %s");
      this.label_songs_per_coin.set_text(template.printf(songs_per_coin, coin_cost));
    }

    public void set_songs_left(string songs_left) {
      string template = _("%s songs left to choose");

      this.label_songs_left.set_text(template.printf(songs_left));
    }

    public void set_idle(bool idle) {
      if (idle) {
        this.stack.set_visible_child_name("page_idle");
      } else {
        this.stack.set_visible_child_name("page_player");
      }
    }

    public void playlist_update_songs(ListStore songs) {
      this.playlist.update_songs(songs);
      this.page_idle_cover.set_songs_left(this.playlist.songs_in_playlist());
    }

    public void playlist_update_time(int time_left, int time_elapsed) {
      this.playlist.update_time(time_left, time_elapsed);
      this.page_idle_cover.update_time(time_elapsed);
      this.page_idle_cover.set_total_time_left(time_left);
    }

    public void idle_cover_update(string title, string artist, string path, double length) {
      this.page_idle_cover.update(title, artist, path, length);
    }

    public void set_qr_code(Gdk.Pixbuf? qr_code) {
      this.page_idle_cover.set_qr_code(qr_code);
    }

    public void select_first_album_initial(string initial) {
      this.albums_viewer.select_first_album_initial(initial);
    }

    public void show_message(string text, Jukebox.MessageType type) {
      var message = new Message(text, type);
      this.overlay.add_overlay(message);
      this.page_state = state.MESSAGE;

      int seconds = 1;
      if (type == Jukebox.MessageType.ERROR) {
        seconds = 3;
      }

      Timeout.add_seconds(seconds, () => {
        this.overlay.remove_overlay(message);
        this.page_state = state.PLAYER;
        return Source.REMOVE;
      });
    }

    public void show_dialog_initials(Array<string> initials) {
      this.initials = new DialogInitials(initials);

      this.overlay.add_overlay(this.initials);
      this.page_state = state.DIALOG_INITIALS;

      this.initials.initial_selected.connect((initial) => {
        this.overlay.remove_overlay(this.initials);
        this.select_first_album_initial(initial);
        this.page_state = state.PLAYER;
        this.initials.destroy();
      });
    }

    public void show_dialog_song_selected(Song song) {
      this.dialog_song_selected = new DialogSongSelected(song);

      this.overlay.add_overlay(this.dialog_song_selected);
      this.page_state = state.DIALOG_CONFIRM;
    }

    public void dialog_song_selected_response() {
      this.add_song_response();
      this.overlay.remove_overlay(this.dialog_song_selected);
      this.page_state = state.PLAYER;
      this.dialog_song_selected.destroy();
    }

    public void move_initial_forward() {
      this.initials ? .select_initial(PagePlayer.move.RIGHT);
    }

    public void move_initial_backward() {
      this.initials ? .select_initial(PagePlayer.move.LEFT);
    }

    public void set_initials_selection() {
      this.initials ? .set_selection();
    }

    public void move_album_forward() {
      this.move_album_selection(PagePlayer.move.RIGHT);
    }

    public void move_album_backward() {
      this.move_album_selection(PagePlayer.move.LEFT);
    }

    public void move_song_down() {
      this.move_song_selection(PagePlayer.move.DOWN);
    }

    public void move_song_up() {
      this.move_song_selection(PagePlayer.move.UP);
    }

    public void dialog_confirmation_change_response() {
      this.dialog_song_selected.change_response();
    }

    public enum move {
      DOWN = 1,
      LEFT = -1,
      RIGHT = 1,
      UP = -1
    }

    public enum state {
      PLAYER,
      MESSAGE,
      DIALOG_CONFIRM,
      DIALOG_INITIALS
    }
  }
}
