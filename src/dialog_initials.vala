/* listview_initials.vala
 *
 * Copyright 2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/dialog_initials.ui")]
  public class DialogInitials : Gtk.Widget {
    [GtkChild]
    private unowned Gtk.Box box;
    [GtkChild]
    private unowned Gtk.ListView listview_initials;
    private Gtk.SizeGroup size_group;
    private Settings settings;
    private Gtk.SingleSelection selection;

    public signal void initial_selected(string initial);

    static construct {
      set_css_name("rockarrolla-message-background");
    }

    public DialogInitials(Array<string> initials) {
      this.size_group = new Gtk.SizeGroup(Gtk.SizeGroupMode.BOTH);

      this.listview_initials.activate.connect((position) => { debug("position %u", position); });

      var factory = new Gtk.SignalListItemFactory();
      factory.setup.connect(this.factory_setup);
      factory.bind.connect(this.factory_bind);
      factory.unbind.connect(this.factory_unbind);

      this.listview_initials.factory = factory;

      this.create_model(initials);

      this.settings = new Settings("ar.com.softwareperonista.Rockarrolla");

      this.destroy.connect(() => { this.box.unparent(); });
    }

    private void create_model(Array<string> initials) {
      var liststore = new ListStore(typeof (ObjectString));

      for ( int i = 0; i < initials.length; i++ ) {
        var text = new ObjectString(initials.index(i));
        liststore.append(text);
      }

      this.selection = new Gtk.SingleSelection(liststore);
      this.selection.set_autoselect(false);

      this.listview_initials.set_model(selection);
    }

    private void factory_setup(Object object) {
      var listitem = object as Gtk.ListItem;
      var label = new Gtk.Label("");

      label.set_margin_start(10);
      label.set_margin_end(10);
      label.set_valign(Gtk.Align.CENTER);
      label.set_halign(Gtk.Align.CENTER);
      label.set_vexpand(true);
      label.set_vexpand_set(true);
      label.set_hexpand(true);
      label.set_hexpand_set(true);
      this.size_group.add_widget(label);

      listitem.set_child(label);
    }

    private void factory_bind(Object object) {
      var listitem = object as Gtk.ListItem;
      var initial = listitem.get_item() as ObjectString;
      var label = listitem.get_child() as Gtk.Label;

      if (label != null) {
        label.set_label(initial.text);
        debug(initial.text);
      }
    }

    private void factory_unbind(Object object) {
      var listitem = object as Gtk.ListItem;
      var label = listitem.get_child() as Gtk.Label;

      if (label != null) {
        label.set_text("");
      }
    }

    public void select_initial(int delta) {
      if (delta != 0) {
        var selected = this.selection.get_selected();

        var items_in_list = this.selection.get_model().get_n_items();
        if ((selected + delta) == items_in_list) {
          this.selection.set_selected(0);
        } else {
          if ((selected == 0) && (delta == -1)) {
            this.selection.set_selected(items_in_list - 1);
          } else {
            if (this.selection.get_model().get_item(selected + delta) != null) {
              this.selection.set_selected((selected + delta));
            }
          }
        }
      }
    }

    public void set_selection() {
      var item = this.selection.get_selected_item() as ObjectString;
      this.initial_selected(item.text);
    }
  }

  public class ObjectString : Object {
    public string text { get; construct set; }

    public ObjectString(string text) {
      this.text = text;
    }
  }
}
