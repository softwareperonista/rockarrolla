/* playlist_song.vala
 *
 * Copyright 2022 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class PlaylistSong : Object {
    public Song song { get; private set; }
    public string device_id { get; set; }

    public PlaylistSong(Song song, string device_id) {
      this.song = song;
      this.device_id = device_id;
    }

    public Json.Node to_json(bool show_device_id) {
      var node = new Json.Node(Json.NodeType.OBJECT);
      var object = new Json.Object();

      var device_id_to_show = "";
      if (show_device_id) {
        device_id_to_show = this.device_id;
      }
      object.set_string_member("device_id", device_id_to_show);

      object.set_member("song", this.song.to_json());

      node.init_object(object);

      return node;
    }
  }
}
