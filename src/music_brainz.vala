/* music_brainz.vala
 *
 * Copyright 2020-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class MusicBrainz : Object {
    private const string music_brainz_url = "https://musicbrainz.org/ws/2";
    private const string cover_art_archive_url = "https://coverartarchive.org";
    private Song song;

    public MusicBrainz(Song s) {
      this.song = s;
    }

    public async Gdk.Pixbuf ? get_cover_async() {
      debug(this.song.album);
      var release_id = yield this.get_release_id_async();

      if (release_id == "") {
        return null;
      }

      var cover_stream = yield this.get_cover_data_async(release_id);

      var stream_is_valid = true;
      uint8[] cover_data = new uint8[10485760];
      try {
        yield cover_stream.read_all_async(cover_data, 100, null, null);

        var cover_string = (string) cover_data;
        if (cover_string.contains("Not Found")) {
          stream_is_valid = false;
          debug("response: 'not found' page");
        } else {
          cover_stream = new MemoryInputStream.from_data(cover_data);
        }
      } catch (Error e) {
        debug(e.message);
      }


      Gdk.Pixbuf pixbuf = null;
      if (stream_is_valid) {
        try {
          pixbuf = new Gdk.Pixbuf.from_stream(cover_stream, null);
        } catch (Error e) {
          message(e.message);
        }
      }

      return pixbuf;
    }

    private async string get_release_id_async() {
      var url = MusicBrainz.music_brainz_url + "/release?query=\"%s\" AND artist:\"%s\"&limit=1".printf(this.song.album, this.song.artist);

      var response_stream = yield this.get_data_from_url_async(url);

      uint8[] response_data = new uint8[10485760];
      try {
        yield response_stream.read_all_async(response_data, 100, null, null);
      } catch (Error e) {
        debug(e.message);
      }

      var data = parse_response((string) response_data, "release");
      if (data.length == 0) {
        return "";
      }

      var release_id = data.item(0).get_attribute("id");

      return release_id;
    }

    private async InputStream get_cover_data_async(string release_id) {
      var url = MusicBrainz.cover_art_archive_url + "/release/%s/front-500".printf(release_id);
      debug(url);

      var response = yield this.get_data_from_url_async(url);

      return response;
    }

    private async InputStream get_data_from_url_async(string uri) {
      var session = new Soup.Session();
      var message = new Soup.Message("GET", uri);

      session.user_agent = "Rockarrolla/%s (https://gitlab.com/softwareperonista/rockarrolla)".printf(Config.VERSION);

      InputStream response = new MemoryInputStream();
      try {
        response = yield session.send_async(message, 100, null);
      } catch (Error e) {
        warning(e.message);
      }

      return response;
    }

    private GXml.DomHTMLCollection parse_response(string xml, string tag_name) {
      GXml.DomHTMLCollection elements = new GXml.DomElementList();

      try {
        var doc = new GXml.Document.from_string(xml);
        elements = doc.get_elements_by_tag_name(tag_name);
      } catch (Error e) {
        message(e.message);
      }

      return elements;
    }
  }
}
