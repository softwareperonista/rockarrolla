/* collection.vala
 *
 * Copyright 2022 Andrés Fernández <andres@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/dialog_song_selected.ui")]
  public class DialogSongSelected : Gtk.Widget {
    [GtkChild]
    private unowned Gtk.Label label_song;
    [GtkChild]
    private unowned Gtk.Button button_no;
    [GtkChild]
    private unowned Gtk.Button button_yes;
    [GtkChild]
    private unowned Gtk.Box box;
    public bool confirmed { get; private set; }

    public signal void on_response();

    static construct {
      set_css_name("rockarrolla-message-background");
    }

    public DialogSongSelected(Song song) {
      this.label_song.set_label(song.title);
      this.notify["confirmed"].connect(this.on_change_response);
      this.confirmed = true;
      this.destroy.connect(() => { this.box.unparent(); });
    }

    public void change_response() {
      if (this.confirmed) {
        this.confirmed = false;
      } else {
        this.confirmed = true;
      }
    }

    [GtkCallback]
    private void on_clicked_button_no() {
      this.confirmed = false;
      this.on_response();
    }

    [GtkCallback]
    private void on_clicked_button_yes() {
      this.confirmed = true;
      this.on_response();
    }

    private void on_change_response() {
      if (this.confirmed) {
        this.button_no.remove_css_class("suggested-action");
        this.button_yes.add_css_class("suggested-action");
      } else {
        this.button_yes.remove_css_class("suggested-action");
        this.button_no.add_css_class("suggested-action");
      }
    }
  }
}
