/* server_handler.vala
 *
 * Copyright 2022 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class ServerHandler : Object {
    private weak Soup.ServerCallback handler;

    public ServerHandler(Soup.ServerCallback handler) {
      this.handler = handler;
    }

    public unowned Soup.ServerCallback get_handler() {
      return this.handler;
    }
  }
}
