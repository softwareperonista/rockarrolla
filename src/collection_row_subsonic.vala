/* collection_row_subsonic.vala
 *
 * Copyright 2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/collection_row_subsonic.ui")]
  public class CollectionRowSubsonic : CollectionRow {
    [GtkChild]
    private unowned Gtk.Button button_subsonic_server_check_status;
    [GtkChild]
    private unowned Gtk.MenuButton menu_button_subsonic_server;
    [GtkChild]
    private unowned Gtk.Entry entry_name;
    [GtkChild]
    private unowned Gtk.Entry entry_host;
    [GtkChild]
    private unowned Gtk.Entry entry_port;
    [GtkChild]
    private unowned Gtk.Entry entry_username;
    [GtkChild]
    private unowned Gtk.PasswordEntry entry_password;

    construct {
      this.entry_name.notify["text"].connect(() => { this.collection_changed(); });
      this.entry_host.notify["text"].connect(() => { this.collection_changed(); });
      this.entry_port.notify["text"].connect(() => { this.collection_changed(); });
      this.entry_username.notify["text"].connect(() => { this.collection_changed(); });
      this.entry_password.notify["text"].connect(() => { this.collection_changed(); });
    }

    public override void from_string(string data) {
      var splited_data = data.split("|");

      if (splited_data.length == 6) {
        this.menu_button_subsonic_server.set_label(splited_data[1]);
        this.entry_name.text = splited_data[1];
        this.entry_host.text = splited_data[2];
        this.entry_port.text = splited_data[3];
        this.entry_username.text = splited_data[4];
        this.entry_password.text = splited_data[5];

        this.check_collection_status();
      }
    }

    public override string to_string() {
      string retorno = CollectionType.SUBSONIC.to_string() + "|"
        + this.entry_name.text + "|"
        + this.entry_host.text + "|"
        + this.entry_port.text + "|"
        + this.entry_username.text + "|"
        + this.entry_password.text;

      return retorno;
    }

    [GtkCallback]
    public void on_popover_closed() {
      this.menu_button_subsonic_server.set_label(this.entry_name.text);
      this.check_collection_status();
    }

    [GtkCallback]
    private void check_collection_status() {
      var collection = new CollectionSubsonic.from_string(this.to_string());

      collection.check_collection_status.begin((obj, res) => {
        var status = collection.check_collection_status.end(res);

        if (status == CollectionSubsonic.ResponseStatus.OK) {
          this.button_subsonic_server_check_status.set_visible(true);
          this.button_subsonic_server_check_status.set_icon_name("emblem-default-symbolic");
          this.button_subsonic_server_check_status.set_tooltip_text("Server operational");
        } else {
          this.button_subsonic_server_check_status.set_visible(true);
          this.button_subsonic_server_check_status.set_icon_name("edit-delete-symbolic");
          this.button_subsonic_server_check_status.set_tooltip_text(status.to_string());
        }
      });
    }
  }
}
