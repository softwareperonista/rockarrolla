/* collection_row.vala
 *
 * Copyright 2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/collection_row.ui")]
  public abstract class CollectionRow : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Button button_delete_collection;

    public signal void collection_changed();

    public abstract void from_string(string data);
    public abstract string to_string();

    public void set_visible_delete_button(bool visible) {
      this.button_delete_collection.set_sensitive(visible);
      this.button_delete_collection.set_visible(visible);
    }

    [GtkCallback]
    protected void on_button_delete_clicked() {
      var listbox = this.get_ancestor(typeof (Gtk.ListBox)) as Gtk.ListBox;
      listbox.remove(this.get_ancestor(typeof (Gtk.ListBoxRow)) as Gtk.ListBoxRow);
    }
  }
}
