/* cover_fetcher.vala
 *
 * Copyright 2019-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class CoverFetcher {
    private Song song;
    private string cover_path;
    private Doctrina.Infraestructura infra;
    private Gee.HashSet<string> albums_searched;

    public CoverFetcher(Doctrina.Infraestructura infra) {
      this.infra = infra;
      this.song = null;
      this.cover_path = "";
      this.albums_searched = new Gee.HashSet<string> ((v) => { return v.hash(); }, (a, b) => { return a == b; });
    }

    public void set_song(Song song) {
      this.song = song;
      this.cover_path = "";
    }

    public async string fetch_async() {
      if (this.song == null) {
        return "";
      }

      string dir = this.infra.directorio_de_configuracion() + "/albums_covers";
      string filename = "%s.png".printf(CoverFetcher.sha1("%s_%s".printf(this.song.artist, this.song.album)));

      if (Doctrina.Utiles.existe_path("%s/%s".printf(dir, filename))) {
        this.cover_path = "%s/%s".printf(dir, filename);
      }

      if (this.cover_path == "") {
        yield this.get_cover_from_song();
      }

      if (this.cover_path == "" && this.song.uri.contains("file:/")) {
        yield this.get_cover_from_folder_async();
      }

      if (this.cover_path == "") {
        yield this.get_cover_from_musicbrainz();
      }

      return this.cover_path;
    }

    private async void get_cover_from_song() {
      if (song.cover != null) {
        this.save_cover_file(song.cover);
      }
    }

    private async void get_cover_from_folder_async() {
      var file = File.new_for_uri(this.song.uri);
      string album_dir = Path.get_dirname(file ? .get_path());
      string[] filenames = { "folder.jpg", "Folder.jpg", "album.jpg", "cover.jpg" };

      for ( int i = 0; i < filenames.length; i++ ) {
        var cover = File.new_for_path("%s/%s".printf(album_dir, filenames[i]));
        debug("%s/%s: %s", album_dir, filenames[i], cover.query_exists().to_string());
        if (cover.query_exists()) {
          try {
            var pixbuf = new Gdk.Pixbuf.from_file("%s/%s".printf(album_dir, filenames[i]));
            this.save_cover_file(pixbuf);
            break;
          } catch (Error e) {
            error("loading image file: %s", e.message);
          }
        }

        Idle.add(this.get_cover_from_folder_async.callback);
        yield;
      }
    }

    private async void get_cover_from_musicbrainz() {
      if (this.albums_searched.contains(this.song.album)) {
        return;
      }

      var music_brainz = new MusicBrainz(this.song);

      var pixbuf = yield music_brainz.get_cover_async();

      if (pixbuf != null) {
        save_cover_file(pixbuf);
      }

      this.albums_searched.add(this.song.album);
    }

    public async void save_cover_from_stream(InputStream stream) {
      try {
        var pixbuf = yield new Gdk.Pixbuf.from_stream_async(stream);
        this.save_cover_file(pixbuf);
      } catch (Error e) {
        debug("error creating pixbuf: %s", e.message);
      }
    }

    private void save_cover_file(Gdk.Pixbuf pixbuf) {
      string dir = "%s/albums_covers".printf(this.infra.directorio_de_configuracion());
      string filename = "%s.png".printf(CoverFetcher.sha1("%s_%s".printf(this.song.artist, this.song.album)));

      try {
        this.cover_path = "%s/%s".printf(dir, filename);
        var scaled = pixbuf.scale_simple(500, 500, Gdk.InterpType.BILINEAR);
        debug(this.cover_path);
        scaled.save(this.cover_path, "png");
      } catch (Error e) {
        debug("error while saving pixbuf in a file: %s", e.message);
      }
    }

    public static string sha1(string name) {
      string ret = Checksum.compute_for_string(ChecksumType.SHA1, name);
      return ret;
    }
  }
}
