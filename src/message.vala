/* message.vala
 *
 * Copyright 2022 Andres Fernández <andres@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/message.ui")]
  public class Message : Gtk.Widget {
    [GtkChild]
    private unowned Gtk.Label label_message;


    static construct {
      set_css_name("rockarrolla-message-background");
    }

    public Message(string text, Jukebox.MessageType type) {
      this.set_message(text);

      if (type == Jukebox.MessageType.ERROR) {
        this.label_message.add_css_class("rockarrolla-message-error");
      } else {
        this.label_message.add_css_class("rockarrolla-message-text");
      }

      this.destroy.connect(() => { this.label_message.unparent(); });
    }

    public void set_message(string text) {
      this.label_message.set_label(text);
    }
  }
}
