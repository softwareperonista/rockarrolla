/* collection_subsonic.vala
 *
 * Copyright 2021-2023 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class CollectionSubsonic : Object, Collection {
    private string name;
    private string host;
    private string port;
    private string user;
    private string password;

    public CollectionSubsonic() {
    }

    public CollectionSubsonic.from_string(string data) {
      var splited_data = data.split("|");

      if (splited_data.length == 6) {
        this.name = splited_data[1];
        this.host = splited_data[2];
        this.port = splited_data[3];
        this.user = splited_data[4];
        this.password = splited_data[5];
      }
    }

    private string build_uri(string method, string parameters) {
      var salt = (Random.int_range(1000000000, int.MAX)).to_string();
      var token = Checksum.compute_for_string(ChecksumType.MD5, password + salt);

      if (this.host == "" || this.port == "") {
        return "";
      }

      var connector = "";
      if (parameters != "") {
        connector = "&";
      }

      var uri = "http://%s:%s/rest/%s?u=%s&t=%s&s=%s&v=1.15.0&c=rockarrolla%s%s".printf(this.host, this.port, method, this.user, token, salt, connector, parameters);

      debug(uri);
      return uri;
    }

    public async ResponseStatus check_collection_status() {
      ResponseStatus status;
      var uri = this.build_uri("ping", "");

      if (uri == "") {
        return ResponseStatus.CONNECTION_REFUSED;
      }

      var response = yield get_response_string(uri);

      if (response != "") {
        if (response.contains("status=\"ok\"")) {
          status = ResponseStatus.OK;
        } else {
          var error = this.parse_response(response, "error");

          var code = error.item(0).get_attribute("code");

          status = ResponseStatus.from_code_number(int.parse(code));
        }
      } else {
        status = ResponseStatus.CONNECTION_REFUSED;
      }

      return status;
    }

    async InputStream get_response(string uri) {
      var soup_message = new Soup.Message("GET", uri);

      var session = new Soup.Session();

      InputStream ret = null;

      try {
        ret = yield session.send_async(soup_message, 0, null);
      } catch (Error e) {
        message(uri);
        message(e.message);
      }

      return ret;
    }

    async string get_response_string(string uri) {
      uint8[] ret = new uint8[1048576];
      var response = yield this.get_response(uri);

      if (response != null) {
        try {
          yield response.read_all_async(ret, 100, null, null);
        } catch (Error e) {
          message(e.message);
        }
      }

      return (string) ret;
    }

    GXml.DomHTMLCollection parse_response(string xml, string tag_name) {
      GXml.DomHTMLCollection elements = new GXml.DomElementList();

      try {
        var doc = new GXml.Document.from_string(xml);
        elements = doc.get_elements_by_tag_name(tag_name);
      } catch (Error e) {
        message(e.message);
      }

      return elements;
    }

    public async Array<Song> get_songs() {
      var songs = new Array<Song> ();
      string status = _("Getting songs information");
      var albums = yield this.get_albums_async();

      for ( int i = 0; i < albums.length; i++ ) {
        var album = albums.item(i);
        var album_name = album.get_attribute("name").replace("\"", "");
        var artist = album.get_attribute("artist");
        this.loader_update(status, _("Getting songs from %s - %s").printf(artist, album_name), (float) Jukebox.Pulse.START);

        var album_subsonic_id = album.get_attribute("id");
        var album_uri = this.build_uri("getAlbum", "id=%s".printf(album_subsonic_id));

        var album_songs = parse_response(yield get_response_string(album_uri), "song");

        for (int j = 0; j < album_songs.length; j++) {
          var album_song = album_songs.item(j);
          var title = album_song.get_attribute("title").replace("\"", "");
          var track_string = album_song.get_attribute("track");
          if (track_string == null) {
            track_string = "";
          }
          var track = int.parse(track_string);
          var duration = album_song.get_attribute("duration");
          if (duration == null) {
            duration = "";
          }
          var length = int.parse(duration);
          var uri = this.build_uri("download", "id=%s".printf(album_song.get_attribute("id")));

          Gdk.Pixbuf cover = null;
          var cover_art_id = album.get_attribute("coverArt");
          if (cover_art_id != null) {
            var cover_uri = this.build_uri("getCoverArt", "id=%s&size=500".printf(cover_art_id));

            var stream = yield get_response(cover_uri);

            try {
              cover = yield new Gdk.Pixbuf.from_stream_async(stream);
            } catch (Error e) {
              printerr(e.message);
            }
          }

          var song = (new SongBuilder())
             .with_title(title)
             .with_album(album_name)
             .with_artist(artist)
             .with_length(length)
             .with_track(track)
             .with_uri(uri)
             .with_cover(cover)
             .build();
          songs.append_val(song);

          Idle.add(this.get_songs.callback);
          yield;
        }
      }
      this.loader_update(_("Songs information loaded"), "", (float) Jukebox.Pulse.STOP);

      Idle.add(this.get_songs.callback);
      yield;
      return songs;
    }

    private async GXml.DomHTMLCollection get_albums_async() {
      var offset = 0;
      var basic_parameters = "type=alphabeticalByArtist&size=500";
      GXml.DomHTMLCollection elements = new GXml.DomElementList();
      GXml.DomHTMLCollection response = new GXml.DomElementList();

      do {
        var parameters = basic_parameters;
        if (offset != 0) {
          parameters += "&offset=%s".printf(offset.to_string());
        }

        var uri = this.build_uri("getAlbumList2", parameters);

        response = parse_response(yield get_response_string(uri), "album");

        elements.add_all(response);

        offset += response.length;
      } while (response.length > 0);

      return elements;
    }

    public async bool is_song_in_collection_async(string song_uri) {
      var splited_song_uri = song_uri.split("=");
      var id = splited_song_uri[splited_song_uri.length - 1];

      var uri = this.build_uri("getSong", "id=%s".printf(id));

      var song = parse_response(yield get_response_string(uri), "song");

      return song.length == 1;
    }

    public string to_string() {
      return "%s|%s|%s|%s|%s".printf(CollectionType.SUBSONIC.to_string(), this.host, this.port, this.user, this.password);
    }

    public enum ResponseStatus {
      OK,
      GENERIC,
      PARAMETER_MISSING,
      CLIENT_INCOMPATIBLE,
      SERVER_INCOMPATIBLE,
      AUTENTICATION_ERROR,
      TOKEN_FOR_LDAP_USER,
      UNAUTORIZED_OPERATION,
      TRIAL_PERIOD_EXPIRED,
      DATA_NOT_FOUND,
      CONNECTION_REFUSED;

      public string to_string() {
        switch (this) {
        case OK:
          return "ok";
        case GENERIC:
          return _("A generic error.");
        case PARAMETER_MISSING:
          return _("Required parameter is missing.");
        case CLIENT_INCOMPATIBLE:
          return _("Incompatible Subsonic REST protocol version. Client must upgrade.");
        case SERVER_INCOMPATIBLE:
          return _("Incompatible Subsonic REST protocol version. Server must upgrade.");
        case AUTENTICATION_ERROR:
          return _("Wrong username or password.");
        case TOKEN_FOR_LDAP_USER:
          return _("Token authentication not supported for LDAP users.");
        case UNAUTORIZED_OPERATION:
          return _("User is not authorized for the given operation.");
        case TRIAL_PERIOD_EXPIRED:
          return _("The trial period for the Subsonic server is over. Please upgrade to Subsonic Premium. Visit subsonic.org for details.");
        case DATA_NOT_FOUND:
          return _("The requested data was not found.");
        case CONNECTION_REFUSED:
          return _("Could not connect: Connection refused");
        default:
          return _("A generic error.");
        }
      }

      public static ResponseStatus from_code_number(int code) {
        switch (code) {
        case 0:
          return ResponseStatus.GENERIC;
        case 10:
          return ResponseStatus.PARAMETER_MISSING;
        case 20:
          return ResponseStatus.CLIENT_INCOMPATIBLE;
        case 30:
          return ResponseStatus.SERVER_INCOMPATIBLE;
        case 40:
          return ResponseStatus.AUTENTICATION_ERROR;
        case 41:
          return ResponseStatus.TOKEN_FOR_LDAP_USER;
        case 50:
          return ResponseStatus.UNAUTORIZED_OPERATION;
        case 60:
          return ResponseStatus.TRIAL_PERIOD_EXPIRED;
        case 70:
          return ResponseStatus.DATA_NOT_FOUND;
        default:
          return ResponseStatus.GENERIC;
        }
      }
    }
  }
}
