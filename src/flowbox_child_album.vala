/* flowbox_child_album.vala
 *
 * Copyright 2019-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/flowbox_child_album.ui")]
  public class FlowBoxChildAlbum : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Picture cover;
    [GtkChild]
    private unowned Gtk.Label label_artist_name;
    [GtkChild]
    private unowned Gtk.Label label_name;
    private string id;
    private bool dummy;
    private Album album;

    public FlowBoxChildAlbum() {
      this.dummy = false;
    }

    public void set_album(Album album) {
      this.album = album;
      this.set_artist_name(album.artist);
      this.set_album_name(album.name);
      this.set_id(album.id);
      this.set_cover(album.cover);
    }

    public Album get_album() {
      return album;
    }

    public void set_id(string id) {
      this.id = id;
    }

    public string get_id() {
      return this.id;
    }

    public void set_cover(string path) {
      if (path != "" && Doctrina.Utiles.existe_path(path)) {
        this.cover.set_filename(path);
      } else {
        this.cover.set_resource("/ar/com/softwareperonista/rockarrolla/assets/no_cover.svg");
      }
    }

    public void set_artist_name(string name) {
      this.label_artist_name.set_label(name);
    }

    public void set_album_name(string name) {
      this.label_name.set_label(name);
    }

    public void set_dummy() {
      this.dummy = true;
      this.set_album_name(" ");
      this.set_artist_name(" ");
    }

    public bool is_dummy() {
      return this.dummy;
    }
  }
}
