/* collection_type.vala
 *
 * Copyright 2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public enum CollectionType {
    FOLDER,
    SUBSONIC;

    public static CollectionType parse(string data) {
      CollectionType ret = CollectionType.FOLDER;

      var splited_data = data.split("|");

      if (splited_data.length > 0) {
        var type = splited_data[0];
        if (type == CollectionType.SUBSONIC.to_string()) {
          ret = CollectionType.SUBSONIC;
        }
      }

      return ret;
    }

    public string to_string() {
      var type = "";

      switch (this) {
      case FOLDER:
        type = "folder";
        break;
      case SUBSONIC:
        type = "subsonic";
        break;
      }

      return type;
    }
  }
}
