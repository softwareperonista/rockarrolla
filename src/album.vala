/* album.vala
 *
 * Copyright 2020-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Album : Object {
    public string id { get; private set; }
    public string name { get; private set; }
    public string artist { get; private set; }
    public string cover { get; private set; }
    public uint songs_in_album { get; private set; }
    public ListStore songs { get; private set; }

    public Album(string id, string name, string artist, string cover) {
      this.id = id;
      this.name = name;
      this.artist = artist;
      this.cover = cover;

      this.songs = new ListStore(typeof (Song));
    }

    public void add_song(Song new_song) {
      this.songs.append(new_song);
    }

    public void set_total_songs(uint songs_in_album) {
      this.songs_in_album = songs_in_album;
    }

    public string get_initial() {
      int len = 1;

      while (!this.artist.valid_char(len)) {
        len++;
      }

      return this.artist.substring(0, len).up();
    }

    public Json.Node to_json() {
      var node = new Json.Node(Json.NodeType.OBJECT);
      var object = new Json.Object();

      object.set_string_member("id", this.id);
      object.set_string_member("name", this.name);
      object.set_string_member("artist", this.artist);

      var cover_split = this.cover.split("/");
      if (cover_split.length != 0) {
        var cover = cover_split[cover_split.length - 1];
        object.set_string_member("cover", cover);
      }

      var songs = new Json.Array();
      for (int i = 0; i < this.songs.get_n_items(); i++) {
        var song = this.songs.get_item(i) as Song;

        songs.add_element(song.to_json());
      }

      object.set_array_member("songs", songs);

      node.init_object(object);

      return node;
    }
  }
}
