/* song.vala
 *
 * Copyright 2020-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Song : Object {
    public string id { get; private set; }
    public string title { get; private set; }
    public int length { get; private set; }
    public string artist { get; private set; }
    public string album { get; private set; }
    public int track { get; private set; }
    public string uri { get; private set; }
    public Gdk.Pixbuf cover { get; private set; }
    public string cover_filename { get; private set; }

    public Song(string id, string title, int length, string artist, string album, int track, string uri, Gdk.Pixbuf? cover, string cover_filename) {
      this.id = id;
      this.title = title;
      this.length = length;
      this.artist = artist;
      this.album = album;
      this.track = track;
      this.uri = uri;
      this.cover = cover;
      this.cover_filename = cover_filename;
    }

    public Json.Node to_json() {
      var node = new Json.Node(Json.NodeType.OBJECT);
      var object = new Json.Object();

      object.set_string_member("id", this.id);
      object.set_string_member("title", this.title);
      object.set_string_member("artist", this.artist);
      object.set_int_member("length", this.length);
      object.set_string_member("cover", this.cover_filename);

      node.init_object(object);

      return node;
    }
  }
}
