/* song_builder.vala
 *
 * Copyright 2022 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class SongBuilder {
    private string id;
    private string title;
    private int length;
    private string artist;
    private string album;
    private int track;
    private string uri;
    private Gdk.Pixbuf cover;
    private string cover_filename;

    public SongBuilder() {
      this.id = "";
      this.title = "";
      this.length = 0;
      this.artist = "";
      this.album = "";
      this.track = 0;
      this.uri = "";
      this.cover = null;
      this.cover_filename = "";
    }

    public SongBuilder with_id(string id) {
      this.id = id;
      return this;
    }

    public SongBuilder with_title(string title) {
      this.title = title;
      return this;
    }

    public SongBuilder with_length(int length) {
      this.length = length;
      return this;
    }

    public SongBuilder with_artist(string artist) {
      this.artist = artist;
      return this;
    }

    public SongBuilder with_album(string album) {
      this.album = album;
      return this;
    }

    public SongBuilder with_track(int track) {
      this.track = track;
      return this;
    }

    public SongBuilder with_uri(string uri) {
      this.uri = uri;
      return this;
    }

    public SongBuilder with_cover(Gdk.Pixbuf? cover) {
      this.cover = cover;
      return this;
    }

    public SongBuilder with_cover_filename(string cover_filename) {
      var cover_split = cover_filename.split("/");
      if (cover_split.length != 0) {
        var cover = cover_split[cover_split.length - 1];
        this.cover_filename = cover;
      }

      return this;
    }

    public Song build() {
      return new Song(this.id,
                      this.title,
                      this.length,
                      this.artist,
                      this.album,
                      this.track,
                      this.uri,
                      this.cover,
                      this.cover_filename);
    }
  }
}
