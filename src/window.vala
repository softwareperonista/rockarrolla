/* application_window.vala
 *
 * Copyright 2018-2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/window.ui")]
  public class Window : Adw.Window {
    [GtkChild]
    private unowned Adw.ViewStack stack_main;
    [GtkChild]
    private unowned PageConfiguration page_configuration;
    [GtkChild]
    private unowned PagePlayer page_player;
    [GtkChild]
    private unowned PageSongsLoader page_songs_loader;
    [GtkChild]
    private unowned Adw.ToolbarView toolbar_view;
    private Jukebox jukebox;
    private int previous_height;
    private uint timeout_idle_id;
    private uint timeout_first_load_id;
    private uint initials_counter;
    private Gtk.EventControllerKey event_controller_key;
    private Settings settings;
    private bool fullscreen_setted;
    private bool coin_key_released;
    private bool party_mode;

    public Window(Gtk.Application app) {
      Object(application: app);
      this.settings = new Settings("ar.com.softwareperonista.Rockarrolla");

      this.create_main_menu();

      this.event_controller_key = new Gtk.EventControllerKey();
      this.event_controller_key.key_pressed.connect(this.on_key_press_event);
      this.event_controller_key.key_released.connect(this.on_key_release_event);
      this.event_controller_key.set_propagation_phase(Gtk.PropagationPhase.CAPTURE);
      var widget = this as Gtk.Widget;
      widget.add_controller(this.event_controller_key);

      Environment.set_prgname("rockarrolla");
      Environment.set_application_name("Rockarrolla");
      Gtk.Window.set_default_icon_name("ar.com.softwareperonista.Rockarrolla");

      // remove field never used warning
      // without the field doesn't find the page
      this.page_configuration.get_visible();

      var css_provider = new Gtk.CssProvider();
      css_provider.load_from_resource("/ar/com/softwareperonista/rockarrolla/ui/rockarrolla.css");
      var display = Gdk.Display.get_default();
      // Gtk.StyleContext is deprecated but add_provider_for_display() not this is a false positive
      Gtk.StyleContext.add_provider_for_display(display, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

      this.previous_height = 0;
      this.initials_counter = 0;

      this.coin_key_released = true;

      this.jukebox = new Jukebox();
      this.jukebox.albums_load_start.connect(this.albums_load_start);
      this.jukebox.album_loaded.connect(this.load_album);
      this.jukebox.albums_load_end.connect(this.albums_load_end);
      this.jukebox.playlist_empty.connect(this.playlist_empty);
      this.jukebox.playlist_update_songs.connect(this.page_player.playlist_update_songs);
      this.jukebox.playlist_update_time.connect(this.page_player.playlist_update_time);
      this.jukebox.idle_cover_update.connect(this.page_player.idle_cover_update);
      this.jukebox.loader_update.connect(this.page_songs_loader.update_progress);
      this.jukebox.collection_reloaded.connect(this.page_songs_loader.reload_finished);
      this.jukebox.updated_songs_left.connect(this.page_player.set_songs_left);
      this.jukebox.update_songs_per_coin.connect(this.page_player.set_songs_per_coin);
      this.jukebox.message.connect(this.page_player.show_message);
      this.jukebox.party_mode.connect(this.party_mode_handler);
      this.page_player.add_song_response.connect(this.add_song_on_response);

      this.jukebox.update_info_labels();

      this.page_songs_loader.reload_collection.connect(this.jukebox.reload_collection);

      this.timeout_first_load_id = Timeout.add_seconds(1, this.first_load);

      this.set_default_fullscreen_mode();
      this.jukebox.check_party_mode();
    }

    private void create_main_menu() {
      var action = new SimpleAction("about", null);
      action.set_enabled(true);
      action.activate.connect(() => { this.about(); });
      this.application.add_action(action);

      action = new SimpleAction("quit", null);
      action.set_enabled(true);
      action.activate.connect(() => { this.close(); });
      this.application.add_action(action);
    }

    private void about() {
      string[] authors = {
        "Andrés Fernández <andres@softwareperonista.com.ar>",
        "Fernando Fernández <fernando@softwareperonista.com.ar>"
      };

      string[] artists = {
        "María Belén Ferreyra <ferreyra.mbel@gmail.com>"
      };

      Gtk.show_about_dialog(this,
                            "authors", authors,
                            "artists", artists,
                            "program-name", "Rockarrolla",
                            "title", _("About") + " Rockarrolla",
                            "comments", _("A jukebox emulator"),
                            "copyright", "Copyright 2019-2022 Fernando Fernández %s Andrés Fernández".printf(_("and")),
                            "license-type", Gtk.License.GPL_3_0,
                            "logo-icon-name", "ar.com.softwareperonista.Rockarrolla",
                            "version", Config.VERSION,
                            "website", "https://gitlab.com/softwareperonista/rockarrolla",
                            "wrap-license", true);
    }

    private bool first_load() {
      this.jukebox.load_albums();
      this.jukebox.restore_playlist();

      this.timeout_idle_id = Timeout.add_seconds(30, this.set_idle);
      return GLib.Source.REMOVE;
    }

    private void albums_load_start() {
      this.page_player.albums_load_start();
      this.page_songs_loader.set_button_reload_sensitive(false);
    }

    private void load_album(Album album) {
      this.page_player.load_album(album);
    }

    private void albums_load_end() {
      this.page_player.albums_load_end();
      this.page_songs_loader.set_button_reload_sensitive(true);
    }

    private bool on_key_press_event(uint keyval, uint keycode, Gdk.ModifierType state) {
      if (!this.party_mode) {
        this.page_player.set_idle(false);
        if (this.timeout_idle_id != 0) {
          Source.remove(this.timeout_idle_id);
          this.timeout_idle_id = 0;
        }
        this.timeout_idle_id = Timeout.add_seconds(30, this.set_idle);
      }

      this.check_initial_counter();
      if (this.stack_main.get_visible_child() == this.page_player) {
        if (this.check_key(keyval, this.settings.get_uint("key-right")) && !this.party_mode) {
          debug("right");
          if (this.page_player.page_state == PagePlayer.state.PLAYER) {
            this.initials_counter++;
            this.page_player.move_album_forward();
          } else {
            if (this.page_player.page_state == PagePlayer.state.DIALOG_CONFIRM) {
              this.page_player.dialog_confirmation_change_response();
            } else {
              if (this.page_player.page_state == PagePlayer.state.DIALOG_INITIALS) {
                this.page_player.move_initial_forward();
              }
            }
          }
        } else {
          if (this.check_key(keyval, this.settings.get_uint("key-left")) && !this.party_mode) {
            debug("left");
            if (this.page_player.page_state == PagePlayer.state.PLAYER) {
              this.initials_counter++;
              this.page_player.move_album_backward();
            } else {
              if (this.page_player.page_state == PagePlayer.state.DIALOG_CONFIRM) {
                this.page_player.dialog_confirmation_change_response();
              } else {
                if (this.page_player.page_state == PagePlayer.state.DIALOG_INITIALS) {
                  this.page_player.move_initial_backward();
                }
              }
            }
          } else {
            if (this.check_key(keyval, this.settings.get_uint("key-up")) && this.page_player.page_state == PagePlayer.state.PLAYER && !this.party_mode) {
              debug("up");
              this.page_player.move_song_up();
            } else {
              if (this.check_key(keyval, this.settings.get_uint("key-down")) && this.page_player.page_state == PagePlayer.state.PLAYER && !this.party_mode) {
                debug("down");
                this.page_player.move_song_down();
              } else {
                if (this.check_key(keyval, this.settings.get_uint("key-play")) && !this.party_mode) {
                  debug("play");
                  if (this.page_player.page_state == PagePlayer.state.PLAYER) {
                    if (this.jukebox.add_song(this.page_player.get_selected_song_id())) {
                      this.page_player.show_dialog_song_selected(this.jukebox.create_song(this.page_player.get_selected_song_id()));
                    }
                  } else {
                    if (this.page_player.page_state == PagePlayer.state.DIALOG_CONFIRM) {
                      this.page_player.dialog_song_selected_response();
                    } else {
                      if (this.page_player.page_state == PagePlayer.state.DIALOG_INITIALS) {
                        this.page_player.set_initials_selection();
                      }
                    }
                  }
                } else {
                  if (this.check_key(keyval, this.settings.get_uint("key-pause")) && this.page_player.page_state == PagePlayer.state.PLAYER) {
                    debug("pause/play");
                    this.jukebox.toggle_pause();
                  } else {
                    if (this.check_key(keyval, this.settings.get_uint("key-skip")) && this.page_player.page_state == PagePlayer.state.PLAYER) {
                      debug("skip song");
                      this.jukebox.skip_song();
                    } else {
                      if (this.check_key(keyval, this.settings.get_uint("key-coin")) && this.page_player.page_state == PagePlayer.state.PLAYER && !this.party_mode) {
                        if (this.coin_key_released) {
                          debug("added song to choose");
                          this.add_songs();
                          this.coin_key_released = false;
                        }
                      } else {
                        if (this.check_key(keyval, this.settings.get_uint("key-configuration")) && this.page_player.page_state == PagePlayer.state.PLAYER) {
                          debug("changed to configuration page");
                          this.stack_main.set_visible_child_name("page_configuration");
                          this.unfullscreen();
                        } else {
                          if (this.check_key(keyval, this.settings.get_uint("key-fullscreen-mode")) && this.page_player.page_state == PagePlayer.state.PLAYER) {
                            debug("changed fullscreen mode");
                            this.toggle_fullscreen_mode();
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        return true;
      }

      return false;
    }

    private void on_key_release_event(uint keyval, uint keycode, Gdk.ModifierType state) {
      if (this.stack_main.get_visible_child() == this.page_player) {
        if (this.check_key(keyval, this.settings.get_uint("key-right")) ||
            this.check_key(keyval, this.settings.get_uint("key-left"))) {
          this.initials_counter = 0;
        } else {
          if (this.check_key(keyval, this.settings.get_uint("key-coin"))) {
            this.coin_key_released = true;
          }
        }
      }
    }

    private void add_song_on_response() {
      if (this.page_player.dialog_song_selected.confirmed) {
        this.jukebox.confirm_add_song_playlist();
      } else {
        this.jukebox.unset_song_to_add_id();
      }
    }

    private void add_songs() {
      this.jukebox.add_songs();
    }

    [GtkCallback]
    private void hide_headerbar() {
      bool visible = true;

      visible &= this.stack_main.get_visible_child_name() != "page_player";
      visible |= !this.is_fullscreen();

      toolbar_view.set_reveal_top_bars(visible);
    }

    private bool set_idle() {
      if (this.jukebox.is_playing() && this.page_player.page_state == PagePlayer.state.PLAYER) {
        this.page_player.set_idle(true);

        if (this.timeout_idle_id != 0) {
          Source.remove(this.timeout_idle_id);
          this.timeout_idle_id = 0;
        }
      }

      return true;
    }

    private void playlist_empty() {
      if (!this.party_mode) {
        this.page_player.set_idle(false);
        if (this.timeout_idle_id != 0) {
          Source.remove(this.timeout_idle_id);
          this.timeout_idle_id = 0;
        }
      } else {
        this.page_player.idle_cover_update("", "", "", 0);
        this.page_player.playlist_update_time(0, 0);
      }
    }

    [GtkCallback]
    private bool on_close_request() {
      this.get_application().quit();

      return true;
    }

    private bool check_key(uint keyval, uint key) {
      uint lower;
      uint upper;
      Gdk.keyval_convert_case(keyval, out lower, out upper);

      return (lower == key) || (upper == key);
    }

    private void check_initial_counter() {
      if (this.initials_counter >= 30) {
        this.show_dialog_initials();
        this.initials_counter = 0;
      }
    }

    private void show_dialog_initials() {
      this.page_player.show_dialog_initials(this.jukebox.get_initials());
    }

    private void toggle_fullscreen_mode() {
      if (this.is_fullscreen()) {
        this.unfullscreen();
        this.fullscreen_setted = false;
        this.get_titlebar().set_visible(true);
      } else {
        this.fullscreen();
        this.fullscreen_setted = true;
      }
    }

    private void set_default_fullscreen_mode() {
      if (this.settings.get_boolean("fullscreen-mode-default")) {
        this.fullscreen();
        this.fullscreen_setted = true;
      }
    }

    private void party_mode_handler(bool enabled, Gdk.Pixbuf? qr_code) {
      this.party_mode = enabled;
      if (enabled) {
        if (this.timeout_idle_id != 0) {
          Source.remove(this.timeout_idle_id);
          this.timeout_idle_id = 0;
        }
        this.page_player.set_qr_code(qr_code);
        this.page_player.set_idle(true);
      } else {
        this.page_player.set_idle(false);
        this.page_player.set_qr_code(null);
      }
    }

    [GtkCallback]
    private void restore_fullscreen() {
      if (this.stack_main.get_visible_child() == this.page_player && this.fullscreen_setted) {
        this.fullscreen();
      }
    }
  }
}
