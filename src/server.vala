/* server.vala
 *
 * Copyright 2022 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Server : Object {
    private uint port;
    private Soup.Server server;
    private Jukebox jukebox;
    private Settings settings;
    private HashTable<string, HashTable<string, ServerHandler>> endpoints;
    private HashTable<string, int> device_last_update;

    public Server(uint port, ref Settings settings, ref Jukebox jukebox) {
      this.jukebox = jukebox;
      this.settings = settings;

      this.endpoints = new HashTable<string, HashTable<string, ServerHandler>> (str_hash, str_equal);
      this.device_last_update = new HashTable<string, int> (str_hash, str_equal);

      this.port = port;

      if (this.settings.get_string("party-mode-id") == "") {
        this.settings.set_string("party-mode-id", Uuid.string_random());
      }

      this.server = new Soup.Server("server-header", "rockarrolla-party-mode", null);

      this.server.add_handler("/", this.default_handler);
    }

    public bool start() {
      message("starting server listening %u...".printf(this.port));
      var ret = false;
      try {
        this.server.listen_all(this.port, Soup.ServerListenOptions.IPV4_ONLY);
        ret = true;
      } catch (Error e) {
        message(e.message);
      }

      if (ret) {
        message("server started successfully");
      }
      return ret;
    }

    public void stop() {
      message("disconnecting server");
      this.server.disconnect();
    }

    public string get_connect_json() {
      var node = new Json.Node(Json.NodeType.OBJECT);
      var object = new Json.Object();
      object.set_string_member("protocol", "http");
      object.set_string_member("host", this.settings.get_string("party-mode-lan-ip"));
      object.set_int_member("port", this.settings.get_int("party-mode-server-port"));
      object.set_string_member("auth_key", this.settings.get_string("party-mode-auth-key"));
      object.set_string_member("id", this.settings.get_string("party-mode-id"));
      object.set_string_member("name", this.settings.get_string("party-mode-server-name"));

      node.init_object(object);

      var gen = new Json.Generator();
      gen.set_pretty(true);
      gen.set_root(node);

      return gen.to_data(null);
    }

    public void add_handler(string path, string method, Soup.ServerCallback handler) {
      if (!this.endpoints.contains(path)) {
        var methods = new HashTable<string, ServerHandler> (str_hash, str_equal);
        this.endpoints.insert(path, (owned) methods);
      }

      var methods = this.endpoints.get(path);
      methods.insert(method, new ServerHandler(handler));
    }

    private void default_handler(Soup.Server server, Soup.ServerMessage msg, string path, HashTable<string, string>? query) {
      if (!this.endpoints.contains(path)) {
        debug("there is no handler for %s", path);
        msg.set_status(404, null);
        return;
      }

      var methods = this.endpoints.get(path);
      if (!methods.contains(msg.get_method())) {
        debug("there is no handler for method %s in route %s", msg.get_method(), path);
        msg.set_status(405, null);
        return;
      }

      if (!this.is_auth_key_valid(msg)) {
        msg.set_status(Soup.Status.UNAUTHORIZED, null);
        return;
      }

      var handler = methods.get(msg.get_method()).get_handler();
      debug("excuting handler for method %s in route %s".printf(msg.get_method(), path));
      handler(server, msg, path, query);
    }

    public void add_handlers() {
      Soup.ServerCallback login_handler = (server, msg, path, query) => {
        var body = msg.get_request_body().flatten();
        var parser = new Json.Parser();
        try {
          parser.load_from_data((string) body.get_data(), (ssize_t) body.get_size());
        } catch (Error e) {
          debug(e.message);
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var root = parser.get_root();
        var object = root.get_object();
        var device_id = object.get_string_member("device_id");

        if (device_id == "") {
          debug("device_id empty");
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        this.jukebox.add_client(device_id);

        msg.set_status(Soup.Status.OK, null);
      };
      this.add_handler("/login", "POST", login_handler);

      Soup.ServerCallback add_song_handler = (server, msg, path, query) => {
        var body = msg.get_request_body().flatten();
        var parser = new Json.Parser();
        try {
          parser.load_from_data((string) body.get_data(), (ssize_t) body.get_size());
        } catch (Error e) {
          debug(e.message);
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var root = parser.get_root();
        var object = root.get_object();

        var device_id = object.get_string_member("device_id");
        if (device_id == "") {
          debug("device_id empty");
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var song_id = object.get_string_member("song_id");
        if (device_id == "") {
          debug("song_id empty");
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        if (!this.jukebox.add_song(song_id, device_id)) {
          debug("can't add song");
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        this.jukebox.confirm_add_song_playlist(device_id);

        msg.set_status(Soup.Status.OK, null);
      };
      this.add_handler("/add_song", "POST", add_song_handler);

      Soup.ServerCallback remove_song_handler = (server, msg, path, query) => {
        var body = msg.get_request_body().flatten();
        var parser = new Json.Parser();
        try {
          parser.load_from_data((string) body.get_data(), (ssize_t) body.get_size());
        } catch (Error e) {
          debug(e.message);
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var root = parser.get_root();
        var object = root.get_object();

        var device_id = object.get_string_member("device_id");
        if (device_id == "") {
          debug("device_id empty");
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var song_id = object.get_string_member("song_id");
        if (device_id == "") {
          debug("song_id empty");
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        if (!this.jukebox.remove_song_waitinglist(song_id, device_id)) {
          debug("Server can't remove song id: '%s' from device: '%s'".printf(song_id, device_id));
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        msg.set_status(Soup.Status.OK, null);
      };
      this.add_handler("/remove_song", "POST", remove_song_handler);

      Soup.ServerCallback albums_handler = (server, msg, path, query) => {
        var albums = this.jukebox.get_albums();

        var root = new Json.Node(Json.NodeType.ARRAY);

        var array = new Json.Array();
        for (int i = 0; i < albums.length; i++) {
          array.add_element(albums.index(i).to_json());
        }

        root.init_array(array);

        var json_generator = new Json.Generator();
        json_generator.set_root(root);

        msg.set_status(Soup.Status.OK, null);
        msg.set_response("application/json", Soup.MemoryUse.COPY, json_generator.to_data(null).data);
      };
      this.add_handler("/albums", "GET", albums_handler);

      Soup.ServerCallback cover_handler = (server, msg, path, query) => {
        debug(msg.get_uri().to_string());

        HashTable<string, string> parameters = null;
        try {
          if (msg.get_uri().get_query() != null) {
            parameters = Uri.parse_params(msg.get_uri().get_query());
          }
        } catch (Error e) {
          msg.set_status(Soup.Status.BAD_REQUEST, e.message);
          return;
        }

        var filename = "";
        if (parameters != null && parameters.contains("id")) {
          filename = parameters.get("id");
        }

        var image_file = File.new_for_path(this.jukebox.covers_dir + filename);

        var image_path = "";
        if (image_file.query_exists() && image_file.query_file_type(FileQueryInfoFlags.NONE) == FileType.REGULAR) {
          image_path = this.jukebox.covers_dir + filename;
        }

        uint8[] data;
        try {
          var pixbuf = this.create_pixbuf(image_path);
          var resized = pixbuf.scale_simple(256, 256, Gdk.InterpType.BILINEAR);
          resized.save_to_buffer(out data, "png");
        } catch (Error e) {
          debug(e.message);
          msg.set_status(Soup.Status.BAD_REQUEST, e.message);
          return;
        }

        msg.set_status(Soup.Status.OK, null);
        msg.set_response("image/png", Soup.MemoryUse.COPY, data);
      };
      this.add_handler("/albums/cover", "GET", cover_handler);

      Soup.ServerCallback playlist_handler = (server, msg, path, query) => {
        HashTable<string, string> parameters = null;
        try {
          parameters = Uri.parse_params(msg.get_uri().get_query());
        } catch (Error e) {
          debug("bad request: No params");
          msg.set_status(Soup.Status.BAD_REQUEST, e.message);
          return;
        }

        if (!parameters.contains("device_id")) {
          debug("bad request: parameter device_id not found");
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var device_id = parameters.get("device_id");

        var playlist = this.jukebox.playlist;

        var root = new Json.Node(Json.NodeType.OBJECT);
        var object = new Json.Object();

        var playlist_array = new Json.Array();
        for (int i = 0; i < playlist.length; i++) {
          var song = playlist.index(i);
          var show_device_id = song.device_id == device_id;
          playlist_array.add_element(song.to_json(show_device_id));
        }
        object.set_array_member("songs", playlist_array);

        var device_waitinglist = this.jukebox.get_device_waitinglist(device_id);
        var device_array = new Json.Array();
        debug(device_waitinglist.length.to_string());
        for (int i = 0; i < device_waitinglist.length; i++) {
          var song = device_waitinglist.index(i);
          device_array.add_element(song.to_json());
        }
        object.set_array_member("waitinglist", device_array);

        var global_waitinglist = this.jukebox.get_global_waitinglist();
        var global_array = new Json.Array();
        debug(global_waitinglist.length.to_string());
        for (int i = 0; i < global_waitinglist.length; i++) {
          var song = global_waitinglist.index(i);
          if (song != null) {
            global_array.add_element(song.to_json());
          }
        }
        object.set_array_member("global_waitinglist", global_array);

        root.init_object(object);

        var json_generator = new Json.Generator();
        json_generator.set_root(root);

        this.device_last_update.insert(device_id, jukebox.get_timestamp());

        debug("ok");
        msg.set_status(Soup.Status.OK, null);
        msg.set_response("application/json", Soup.MemoryUse.COPY, json_generator.to_data(null).data);
      };
      this.add_handler("/playlist", "GET", playlist_handler);

      Soup.ServerCallback playlist_should_update_handler = (server, msg, path, query) => {
        HashTable<string, string> parameters = null;
        try {
          parameters = Uri.parse_params(msg.get_uri().get_query());
        } catch (Error e) {
          msg.set_status(Soup.Status.BAD_REQUEST, e.message);
          return;
        }

        if (!parameters.contains("device_id")) {
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var device_id = parameters.get("device_id");

        if (this.device_last_update.contains(device_id) &&
            this.device_last_update.get(device_id) > jukebox.get_playlist_last_update()) {
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        msg.set_status(Soup.Status.OK, null);
      };
      this.add_handler("/playlist/should_update", "GET", playlist_should_update_handler);

      Soup.ServerCallback playlist_time_handler = (server, msg, path, query) => {
        var time_left = this.jukebox.get_time_left();
        var time_elapsed = this.jukebox.get_time_elapsed();

        var root = new Json.Node(Json.NodeType.OBJECT);
        var object = new Json.Object();

        object.set_int_member("time_left", time_left);
        object.set_int_member("time_elapsed", time_elapsed);

        root.init_object(object);

        var json_generator = new Json.Generator();
        json_generator.set_root(root);

        msg.set_status(Soup.Status.OK, null);
        msg.set_response("application/json", Soup.MemoryUse.COPY, json_generator.to_data(null).data);
      };
      this.add_handler("/playlist/time", "GET", playlist_time_handler);

      Soup.ServerCallback ping_handler = (server, msg, path, query) => {
        HashTable<string, string> parameters = null;
        try {
          parameters = Uri.parse_params(msg.get_uri().get_query());
        } catch (Error e) {
          msg.set_status(Soup.Status.BAD_REQUEST, e.message);
          return;
        }

        if (!parameters.contains("id")) {
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        var id = parameters.get("id");

        if (id != this.settings.get_string("party-mode-id")) {
          msg.set_status(Soup.Status.BAD_REQUEST, null);
          return;
        }

        msg.set_status(Soup.Status.OK, null);
      };
      this.add_handler("/ping", "GET", ping_handler);
    }

    private bool is_auth_key_valid(Soup.ServerMessage msg) {
      var header = msg.get_request_headers();
      var header_secret_key = header.get_one("Auth-Key");

      var auth_key = this.settings.get_string("party-mode-auth-key");

      return header_secret_key == auth_key;
    }

    private Gdk.Pixbuf create_pixbuf(string path) throws GLib.Error {
      if (path == "") {
        return new Gdk.Pixbuf.from_resource("/ar/com/softwareperonista/rockarrolla/assets/no_cover.svg");
      }

      return new Gdk.Pixbuf.from_file(path);
    }
  }
}
