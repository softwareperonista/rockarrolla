/* collection_row.vala
 *
 * Copyright 2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate(ui = "/ar/com/softwareperonista/rockarrolla/ui/collection_row_folder.ui")]
  public class CollectionRowFolder : CollectionRow {
    [GtkChild]
    private unowned Gtk.Button button_songs_directory;
    public string directory { get; private set; }
    const string label_music_folder = "Select Music Folder";

    public CollectionRowFolder() {
      this.notify["directory"].connect(() => {
        this.collection_changed();
      });

      this.button_songs_directory.set_label(CollectionRowFolder.label_music_folder);
    }

    public void set_music_directory(string directory) {
      var file_directory = File.new_for_path(directory);
      this.directory = directory;

      this.set_directory_button(file_directory);
    }

    private void set_directory_button(File directory) {
      var label = _(CollectionRowFolder.label_music_folder);
      if (directory.query_exists()) {
        label = directory.get_basename();
      }

      this.button_songs_directory.set_label(label);
    }

    [GtkCallback]
    private void on_button_songs_directory_clicked() {
      var dialog = new Gtk.FileDialog ();
      /*(,
                                             this.get_ancestor(typeof (Gtk.Window)) as Gtk.Window,
                                             Gtk.FileChooserAction.SELECT_FOLDER,
                                             null, null);
*/
      dialog.set_title(_(CollectionRowFolder.label_music_folder));
      dialog.set_modal(true);
      dialog.select_folder.begin(this.get_ancestor(typeof (Gtk.Window)) as Gtk.Window, null, (obj, response) => {
        try {
          var directory = dialog.select_folder.end(response);
          this.set_music_directory(directory.get_path());
        } catch (Error e) {
          message(e.message);
        }
      });
    }

    public override void from_string(string data) {
      var splited_data = data.split("|");

      if (splited_data.length > 1) {
        this.set_music_directory(splited_data[1]);
      }
    }

    public override string to_string() {
      return "%s|%s".printf(CollectionType.FOLDER.to_string(), this.directory);
    }
  }
}
