/* collection_loader.vala
 *
 * Copyright 2021 Fernando Fernández <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class CollectionLoader : Object {
    private Array<Collection> collections;
    private CoverFetcher fetcher;
    public Doctrina.BaseDeDatos db { get; private set; }
    public Doctrina.Infraestructura infra { get; private set; }

    public signal void loader_update(string status, string action, float progress);

    public CollectionLoader(Doctrina.BaseDeDatos db, Doctrina.Infraestructura infra) {
      this.db = db;
      this.infra = infra;
      this.fetcher = new CoverFetcher(this.infra);
    }

    private void load_collections() {
      this.collections = new Array<Collection> ();
      var settings = new Settings("ar.com.softwareperonista.Rockarrolla");
      var collections = settings.get_strv("collections");

      for ( int i = 0; i < collections.length; i++ ) {
        var collection_string = collections[i];
        var collection = new_collection(collection_string);
        collection.loader_update.connect((status, action, progress) => {
          this.loader_update(status, action, progress);
        });

        this.collections.append_val(collection);
      }
    }

    private Collection new_collection(string data) {
      var type = CollectionType.parse(data);

      switch (type) {
      case CollectionType.SUBSONIC:
        return new CollectionSubsonic.from_string(data);
      default:
        return new CollectionFolder.from_string(data, this.db, this.infra);
      }
    }

    public async void reload_collection_async() {
      this.load_collections();
      var songs = new Array<Song> ();
      for ( int i = 0; i < this.collections.length; i++ ) {
        var collection = this.collections.index(i);
        var collection_songs = yield collection.get_songs();

        for (int j = 0; j < collection_songs.length; j++) {
          var song = collection_songs.index(j);
          songs.append_val(song);
        }
      }

      yield this.insert_albums_async(songs);

      yield this.insert_songs_async(songs);

      yield this.remove_songs_out_of_collections_async();

      yield this.remove_non_existent_songs_async();

      yield this.remove_empty_albums_async();

      yield this.fetch_cover_album_async(songs);

      this.loader_update(_("Collection reloaded"), "", 1);
    }

    private async void insert_albums_async(Array<Song> songs) {
      string status = _("Adding albums");
      string previous_album = "";

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index(i);

        string action = _("Checking %s - %s").printf(song.album, song.title);

        if (song.album != "" && song.album != previous_album && this.db.select("albums", "name", "WHERE name = \"%s\"".printf(song.album)).length == 0) {
          if (song.artist != "") {
            this.db.insert("albums", "name,artist,cover", "\"%s\", \"%s\", \"cover\"".printf(song.album, song.artist));
          }
        }
        previous_album = song.album;

        float progress = (float) i / (float) songs.length;
        this.loader_update(status, action, progress);

        Idle.add(this.insert_albums_async.callback);
        yield;
      }

      this.loader_update(status, _("Done"), 1);
      Idle.add(this.insert_albums_async.callback);
      yield;
    }

    private async void insert_songs_async(Array<Song> songs) {
      string status = _("Adding songs");

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index(i);
        string action = _("Checking %s - %s").printf(song.album, song.title);

        var id = this.db.select("albums", "id", "WHERE name = \"%s\"".printf(song.album)).index(0);

        if (id != null && this.db.select("songs", "title,album,path", "WHERE title = \"%s\" AND album = \"%s\"".printf(song.title, id)).length == 0) {
          this.db.insert("songs", "title,track,album,length,path", "\"%s\", \"%02d\", \"%s\", \"%d\", \"%s\"".printf(song.title, song.track, id, song.length, song.uri));
        }

        float progress = (float) i / (float) songs.length;
        this.loader_update(status, action, progress);
        Idle.add(this.insert_songs_async.callback);
        yield;
      }

      this.loader_update(status, _("Done"), 1);
      Idle.add(this.insert_songs_async.callback);
      yield;
    }

    private async void remove_non_existent_songs_async() {
      string status = _("Removing non existent songs");
      var songs = this.db.select("songs", "id,path,title", "");

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index(i).split("|");
        string action = _("Checking song %s").printf(song[2]);

        if (song[1].contains("file:/")) {
          var file = File.new_for_uri(song[1]);
          var path = file ? .get_path();
          if (!(Doctrina.Utiles.existe_path(path))) {
            this.db.del("songs", "WHERE id=\"%s\"".printf(song[0]));
          }
        }
        float progress = (float) i / (float) songs.length;
        this.loader_update(status, action, progress);
        Idle.add(this.remove_non_existent_songs_async.callback);
        yield;
      }

      this.loader_update(status, _("Done"), 1);
      Idle.add(this.remove_non_existent_songs_async.callback);
      yield;
    }

    private async void remove_songs_out_of_collections_async() {
      string status = _("Removing songs out of music directory");

      var songs = this.db.select("songs", "id,path,title", "");

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index(i).split("|");
        string action = _("Checking song %s").printf(song[2]);

        var path = song[1];
        var delete_song = true;
        for ( int j = 0; j < this.collections.length; j++ ) {
          var collection = this.collections.index(j);
          if (yield collection.is_song_in_collection_async(path)) {
            delete_song = false;
            break;
          }
        }

        if (delete_song) {
          this.db.del("songs", "WHERE id=\"%s\"".printf(song[0]));
        }

        float progress = (float) i / (float) songs.length;
        this.loader_update(status, action, progress);
        Idle.add(this.remove_songs_out_of_collections_async.callback);
        yield;
      }

      this.loader_update(status, _("Done"), 1);
      Idle.add(this.remove_songs_out_of_collections_async.callback);
      yield;
    }

    private async void remove_empty_albums_async() {
      string status = _("Removing empty albums");

      var albums = this.db.select("albums", "id,name", "");

      for ( int i = 0; i < albums.length; i++ ) {
        var album = albums.index(i).split("|");
        string action = _("Checking album %s").printf(album[1]);

        debug("album %s - id: %s", album[1], album[0]);
        if (this.db.select("songs", "id", "WHERE album=\"%s\"".printf(album[0])).length == 0) {
          this.db.del("albums", "WHERE id=\"%s\"".printf(album[0]));
        }

        float progress = (float) i / (float) albums.length;
        this.loader_update(status, action, progress);
        Idle.add(this.remove_empty_albums_async.callback);
        yield;
      }

      this.loader_update(status, _("Done"), 1);
      Idle.add(this.remove_empty_albums_async.callback);
      yield;
    }

    private async void fetch_cover_album_async(Array<Song> songs) {
      string status = _("Searching for album covers");
      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index(i);

        string action = _("Looking for album cover of %s").printf(song.album);

        this.fetcher.set_song(song);

        yield fetcher.fetch_async();

        float progress = (float) i / (float) songs.length;
        this.loader_update(status, action, progress);
        Idle.add(this.fetch_cover_album_async.callback);
        yield;
      }

      this.loader_update(status, "Done", 1);
      Idle.add(this.fetch_cover_album_async.callback);
      yield;
    }
  }
}
